﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Model
{
    public class Pilot : NamedObject
    {
        private Team _Team;
        //private int _Funds;
        private Rank _Rank;
        private PhysicalObject _Target;
        private Ship _Ship;
        private bool _Human;

        public enum Rank
        {
            Cadet,
            Admiral
        }

        public Pilot(string name, Team team, bool human)
        {
            _Name = name;
            _Human = human;
            _Team = team;
            _Rank = Rank.Cadet;

            _Team.AddPilot(this);



        }

        public void SpawnShip()
        {
            if (MyShip() == null)
                SetShip(UniverseController.AddShip(ShipFactory.MakeDreadnaught(Name() + "'s Ship", this)));
        }

        public Ship MyShip()
        {
            return _Ship;
        }

        public void SetShip(Ship ship)
        {
            _Ship = ship;
        }

        public void SetTarget(PhysicalObject tar)
        {
            _Target = tar;
        }

        public Team Team
        {
            get
            {
                return _Team;
            }
        }

        public bool HasTarget()
        {
            if (_Target != null)
            {
                if (_Target is DestroyableObject)
                    if ((_Target as DestroyableObject).Destroyed)
                        return false;

                return true;
            }
            else
            {
                return false;
            }
        }

        public PhysicalObject Target
        {
            get
            {
                if (_Target != null)
                {
                    if (_Target is DestroyableObject)
                        if ((_Target as DestroyableObject).Destroyed)
                            _Target = null;
                }

                return _Target;
            }
        }

        public virtual void Update()
        {

        }
    }
}
