﻿
public class NamedObject {

    protected string _Name;

    public NamedObject()
    {
        _Name = "UNDEFINED";
    }

    public NamedObject(string name)
    {
        _Name = name;
    }

    public virtual void NameObject(string name)
    {
        _Name = name;
    }

	public bool AreYou(string name)
    {
        if (_Name == name)
            return true;
        else
            return false;
    }

    public virtual string Name()
    {
        return _Name;
    }
}
