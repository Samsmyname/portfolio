﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Model.Modules
{
    class Engine : Module
    {
        private int _Power;

        public Engine(int volume, int mass, int power) : base(volume, mass)
        {
            _Power = power;
        }

        public int Power
        {
            get { return _Power; }
        }

        public override void OnUse(Ship ship)
        {
            throw new NotImplementedException();
        }

    }
}
