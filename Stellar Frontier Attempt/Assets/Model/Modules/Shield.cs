﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Model.Modules
{
    class Shield : Module
    {

        private int _ShieldPoints;
        private int _ShieldPointsMax;

        public Shield(int volume, int mass, int maxPoints) : base(volume, mass)
        {
            _ShieldPointsMax = maxPoints;
            _ShieldPoints = _ShieldPointsMax;
        }

        public int ShieldPoints
        {
            get { return _ShieldPoints; }
        }

        public int ShieldPointsMax
        {
            get { return _ShieldPointsMax; }
        }

        public int DamageShield(int dmg)
        {
            if (_ShieldPoints > dmg)
            {
                _ShieldPoints -= dmg;
                return 0;
            }
            else
            {
                dmg -= _ShieldPoints;
                _ShieldPoints = 0;
                return dmg;
            }
        }



        public override void OnUse(Ship ship)
        {
            //Nothing happens
        }

    }
}
