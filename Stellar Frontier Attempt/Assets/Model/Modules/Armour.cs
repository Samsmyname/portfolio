﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Model.Modules
{
    class Armour : Module
    {
        protected int _ArmourPoints;
        private int _ArmourPointsMax;

        public Armour(int volume, int mass, int maxPoints) : base(volume, mass)
        {
            _ArmourPointsMax = maxPoints;
            _ArmourPoints = _ArmourPointsMax;
        }

        public int DamageArmour(int dmg)
        {
            if (_ArmourPoints > dmg)
            {
                _ArmourPoints -= dmg;
                return 0;
            }
            else
            {
                dmg -= _ArmourPoints;
                _ArmourPoints = 0;
                return dmg;
            }
        }

        public int ArmourPoints
        {
            get { return _ArmourPoints; }
        }

        public int ArmourPointsMax
        {
            get { return _ArmourPointsMax; }
        }

        public void SetArmourValue(int value)
        {
            _ArmourPoints = value;
        }

        public override void OnUse(Ship ship)
        {
            //Nothing happens
        }

    }
}
