﻿using UnityEngine;
using System.Collections;

public class Weapon : Module {

    public enum Position
    {
        FrontMiddle,
        FrontLeft,
        FrontRight,
        BackMiddle,
        BackLeft,
        BackRight,
        SideLeft,
        SideRight
    }

    private Position _Position;
    private int _ContactDamage;
    //private int _AreaDamage;
    //private int _AreaRadius;
    private int _Speed;
    private int _LifeSpan;
    private int _ReloadTime;
    private float _TimeTillReload;
    private string _Class;


    public Weapon(int volume, int mass, Position position, int speed, int lifeSpan, int damage, int reloadTime, string c) : base(volume, mass)
    {
        this._Position = position;
        this._Speed = speed;
        this._LifeSpan = lifeSpan;
        this._ContactDamage = damage;
        this._ReloadTime = reloadTime;
        this._TimeTillReload = 0;
        _Class = c;
    }

    public override void OnUse(Ship ship)
    {
        if (_TimeTillReload <= 0)
        {
            UniverseController.OutputText("Firing!");
            Projectile proj = new Projectile(ship, _Speed, _LifeSpan, _ContactDamage, _Class);
            UniverseController.AddProjectile(proj);

            _TimeTillReload = _ReloadTime;
        }
    }

    private void OnHit()
    {

    }

    public override void Update(double deltaTime)
    {
        if (_TimeTillReload > 0)
            _TimeTillReload -= (float)(deltaTime);

        UniverseController.OutputText("Time till reload: " + _TimeTillReload);
    }

}

