﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Model
{
    public interface IListener
    {

        void OnObjectDestroyed(NamedObject obj);
        void OutputText(string text);
        void PlayerChangedTo(Pilot player);
    }
}
