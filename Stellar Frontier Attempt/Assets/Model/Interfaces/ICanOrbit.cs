﻿using Assets.Model;

public interface ICanOrbit {

    OrbitInformation OrbitInfo { get; }
    LocationVector Location();

}
