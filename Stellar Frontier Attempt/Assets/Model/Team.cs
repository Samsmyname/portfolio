﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Model
{
    public class Team : NamedObject
    {
        private int _ID;
        private Dictionary<string, string> _Sprites;
        private static float _PilotRespawnTime = 500;
        private List<Pilot> _Pilots = new List<Pilot>();
        private List<Colony> _Colonies = new List<Colony>();
        private Colony _DefaultHome;
        //private StrategicAi _AI;
        private float _PilotRespawnRemaining = 0;

        public Team(string name, int ID) : base(name)
        {
            _Sprites = new Dictionary<string, string>();
            _ID = ID;
        }

        public void SpawnPilotShip()
        {
            foreach (Pilot p in _Pilots)
            {
                if (p.MyShip() == null)
                {
                    p.SpawnShip();
                    _PilotRespawnRemaining = _PilotRespawnTime;
                    UniverseController.OutputText("Spawning Pilot Ship");
                    break;
                }
            }
        }

        public int ID
        {
            get { return _ID; }
        }

        public void SetSpriteNumber(string n)
        {
            _Sprites.Add("SpaceShip", n);
        }

        public Dictionary<string, string> Sprites
        {
            get { return _Sprites; }
        }

        public void AddPilot(Pilot p)
        {
            _Pilots.Add(p);
        }

        public List<Pilot> Pilots()
        {
            return _Pilots;
        }

        public List<PilotAI> AIPilots()
        {
            List<PilotAI> ai = new List<PilotAI>();

            foreach (Pilot p in _Pilots)
            {
                if (p is PilotAI)
                    ai.Add((p as PilotAI));
            }
            return ai;
        }

        public void AddColony(Colony c)
        {
            _Colonies.Add(c);
        }

        public List<Colony> Colonies()
        {
            return _Colonies;
        }

        public void SetHome(Colony home)
        {
            _DefaultHome = home;
        }

        public Colony HomePlanet()
        {
            if (_DefaultHome != null)
            {
                if (_DefaultHome.Team == this)
                    return _DefaultHome;
                else
                    return _Colonies[0];
            }
            else
            {
                Type[] types = new Type[1];
                types[0] = typeof(Colony);
                return (UniverseController.GetClosestOfType(types, (UniverseController.GetObjectByName("Earth") as PhysicalObject).Location, null) as Colony);
            }
        }

        public void Strategise()
        {
            //_AI.Run();
        }


        public void Update(double deltaTime)
        {
            if (_PilotRespawnRemaining > 0)
                _PilotRespawnRemaining -= (float)deltaTime;

            if (_PilotRespawnRemaining <= 0)
                SpawnPilotShip();

            foreach (Pilot pilot in _Pilots)
            {
                pilot.Update();
            }
        }
    }
}
