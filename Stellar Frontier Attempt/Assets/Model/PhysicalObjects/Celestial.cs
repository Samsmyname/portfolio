﻿using Assets.Model;

public class Celestial : PhysicalObject, ICanOrbitStatic{

    private StaticOrbitInformation _OrbitInfo;
    private Colony _Colony;

    //relating to things that orbit me
    protected float OrbitMeDistance;

    public Celestial() : base()
    {
        _OrbitInfo = new StaticOrbitInformation();   
        OrbitMeDistance = 1;
    }

    public override void Update(double deltaTime)
    {
        if (OrbitInfo.IsOrbiting)
            OrbitInfo.OrbitAdjustment(deltaTime);
    }

    public void SetDefaultOrbitDistance(int distance)
    {
        OrbitMeDistance = distance;
    }

    public Colony AttachedColony()
    {
        return _Colony;
    }

    public void RemoveColony()
    {
        _Colony = null;
    }

    public void PlaceColony(Colony colony)
    {
        _Colony = colony;
    }

    public StaticOrbitInformation OrbitInfo
    {
        get { return _OrbitInfo; }
    }

   public float DefaultOrbitDistance
    {
        get { return OrbitMeDistance; }
    }

    override public LocationVector Location
    {
        get { return _OrbitInfo.Location; }
    }

}
