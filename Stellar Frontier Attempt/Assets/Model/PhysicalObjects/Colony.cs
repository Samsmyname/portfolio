﻿
namespace Assets.Model
{
    public class Colony : ModulerObject
    {

        private Celestial _AttachedTo;
        private float _Population;
        private float _Growth = 0.0001f;
        private float _StartPop;

        public Colony(Team team, int startpop) : base()
        {
            _Team = team;
            _Population = startpop;
            _StartPop = startpop;
        }

        public void AttachTo(Celestial c)
        {
            _AttachedTo = c;
            c.PlaceColony(this);
            SetLocation(c.Location);
        }

        public float Population
        {
            get { return _Population; }
        }
        public float Growth
        {
            get { return _Growth; }
        }


        public override string Name()
        {
            if (_AttachedTo != null)
                return _AttachedTo.Name();
            else
                return _Name;
        }

        protected override void Destroy()
        {
            base.Destroy();

            if (_AttachedTo != null)
            {
                _AttachedTo.RemoveColony();
                _AttachedTo = null;
            }
        }

        public override void Update(double deltaTime)
        {
            if (_AttachedTo != null)
            {
                SetLocation(_AttachedTo.Location);
            }

            if (_Population < _StartPop)
                _Population += (float)(_Population * _Growth * deltaTime);
            
        }


    }
}