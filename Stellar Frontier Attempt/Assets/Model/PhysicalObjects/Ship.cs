﻿using System;
using Assets.Model;
using Assets.Model.Modules;

public class Ship : ModulerObject {

    private Pilot _Pilot;
    private int _Acceleration;
    private float _RotationalSpeed;
    private int _MaxSpeed;
    //private OrbitInformation _OrbitInfo;

    private VelocityVector _CurrentSpeed;

    //relating to things that I orbit
    //private Celestial _OrbitingBody;
    //private float _OrbitDistance;

    public Ship () : base()
    {
        _Acceleration = 1;
        _MaxSpeed = 50;
        _RotationalSpeed = 3;
        _CurrentSpeed = new VelocityVector();

    }

    public override Team Team
    {
        get { return _Pilot.Team; }
    }

    //public OrbitInformation OrbitInfo
    //{
    //   get
    //   {
    //       return _OrbitInfo;
    //   }
    //}

    // private void BreakOrbit()
    //{

    //    if (GameObject.Find("MainCamera").GetComponent<CameraController>().Focus == this)
    //   {
    //     GameObject.Find("MainCamera").GetComponent<CameraController>().FollowShip();    
    //   }

    // _OrbitInfo.BreakOrbit();
    // } 

    public void SetPilot(Pilot p)
    {
        _Pilot = p;
    }


    public void AIMove()
    {
       // LocationVector vector = _TargetLocation - _Location;

       // if (vector.magnitude > 2)
    } 

   // public void EnterOrbit(Celestial planet)
   // {
      //  _OrbitInfo.MakeOrbit(planet);
      //  SetLocation(planet.OrbitInfo.Location);
        //Rigidbody2D rigid = _Go.GetComponent<Rigidbody2D>();
        //rigid.velocity = Vector2.zero;
   // }

    public float CurrentSpeed
    {
        get
        {
            return _CurrentSpeed.Total();
        }
    }

    public VelocityVector CurrentSpeedVector
    {
        get
        {
            return _CurrentSpeed;
        }
    }

    public void RotateLeft()
    {
        // _Go.GetComponent<SpriteRenderer>().transform.Rotate(Vector3.forward * _RotationalSpeed);
        SetAngle(Angle() + _RotationalSpeed);
        CheckAngle();
    }
    public void RotateRight()
    {
        //_Go.GetComponent<SpriteRenderer>().transform.Rotate(Vector3.forward * -_RotationalSpeed);

        SetAngle(Angle() - _RotationalSpeed);
        CheckAngle();
    }

    private float GetAcceleration()
    {
        float acc = _Acceleration;
        foreach (Module m in _Modules)
        {
            if (m is Engine)
            {
                acc += (m as Engine).Power;
            }
        }

        acc /= _Mass;

        return acc;
    }

    public void Accelerate()
    {
        Accelerate(GetAcceleration());
    }

    public void Reverse()
    {
        Accelerate((GetAcceleration()) * -0.5f);
    }

    public void Brake()
    {
        _CurrentSpeed.Limit(Math.Abs(_CurrentSpeed.Magnitude() - 0.05f));
    }

    private void Accelerate(float modifier)
    {   
        _CurrentSpeed.AccelerateInDirection(modifier, Angle(), _MaxSpeed);
    }

    public void FireWeapons()
    {
        foreach (Module module in _Modules)
        {
            if (module is Weapon)
            {
                module.OnUse(this);
            }
        }
    }

    public override void Update(double deltaTime)
    {
        Location.Advance(_CurrentSpeed, deltaTime);
        foreach (Module m in _Modules)
        {
            if (m != null)
                m.Update(deltaTime);
        }
    }



}
