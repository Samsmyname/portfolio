﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Model
{
    public abstract class PhysicalObject : NamedObject
    {

        protected Team _Team;
        protected string _Class;
        private LocationVector _Location;
        private float _Angle;
        private object _Lock = new object();

        // Constructor
        public PhysicalObject( ) : base()
        {
            _Angle = 0;
        }

        public string Class
        {
            get { return _Class; }
        }

        public void SetClass(string c)
        {
            _Class = c;
        }

        protected void CheckAngle()
        {
            if (_Angle > 360)
            {
                _Angle -= 360;
            }
            else if (_Angle < 0)
            {
                _Angle += 360;
            }
        }

        public virtual float Angle()
        {
            lock (_Lock)
            {
                return _Angle;
            }
        }

        public Vector AngleAsVector()
        {
            return Vector.AngleAsVector(Angle());
        }

        public virtual void SetAngle(float angle)
        {
            lock (_Lock)
            {
                _Angle = angle;
            }
        }

        public virtual Team Team
        {
            get { return _Team; }
        }

        public abstract void Update(double deltaTime);

        public virtual LocationVector Location
        {
            
            get {
                lock (_Lock)
                {
                    return _Location;
                }
            }
        }

        public void SetLocation(LocationVector location)
        {
            lock (_Lock)
            {
                _Location = new LocationVector();
                _Location.x = location.x;
                _Location.y = location.y;
            }
        }

    }
}
