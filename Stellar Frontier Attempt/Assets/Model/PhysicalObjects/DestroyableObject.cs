﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Model
{
    public abstract class DestroyableObject : PhysicalObject
    {

        protected int _HullPoints;
        protected int _ArmourPoints;
        protected int _ShieldPoints;
        protected int _HullPointsMax;
        protected int _ArmourPointsMax;
        protected int _ShieldPointsMax;
        private bool _Destroyed;

        public DestroyableObject() : base()
        {
            _HullPointsMax = 100;
            _HullPoints = _HullPointsMax;
            _Destroyed = false;
        }

        protected virtual int DamageShields(int dmg)
        {
            if (_ShieldPoints > dmg)
            {
                _ShieldPoints -= dmg;
                return 0;
            }
            else
            {
                dmg -= _ShieldPoints;
                _ShieldPoints = 0;
            }

            return dmg;
        }

        protected virtual int DamageArmour(int dmg)
        {
            if (_ArmourPoints > dmg)
            {
                _ArmourPoints -= dmg;
                return 0;
            }
            else
            {
                dmg -= _ArmourPoints;
                _ArmourPoints = 0;
            }

            return dmg;
        }

        protected void DamageHull(int dmg)
        {
            if (_HullPoints > dmg)
            {
                _HullPoints -= dmg;
            }
            else
            {
                _HullPoints = 0;
                Destroy();
            }

        }

        public void TakeDamage(int dmg)
        {

            dmg = DamageShields(dmg);
            dmg = DamageArmour(dmg);
            DamageHull(dmg);

        }

        public int HullPoints()
        {
            return _HullPoints;
        }
        public int HullPointsMax()
        {
            return _HullPointsMax;
        }
        public virtual int ArmourPoints()
        {
            return _ArmourPoints;
        }
        public virtual int ArmourPointsMax()
        {
            return _ArmourPointsMax;
        }
        public virtual int ShieldPoints()
        {
            return _ShieldPoints;
        }
        public virtual int ShieldPointsMax()
        {
            return _ShieldPointsMax;
        }

        protected virtual void Destroy()
        {
            _Destroyed = true;
        }

        public bool Destroyed
        {
            get
            {
                return _Destroyed;
            }
        }

    }
}
