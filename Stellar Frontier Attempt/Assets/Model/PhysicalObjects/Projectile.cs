using System;
using Assets.Model;

public class Projectile : DestroyableObject
{

    private VelocityVector _Vector;
    private int _ContactDamage;
    private Ship _Launcher;
    private float _TimeTillDeath;

     public Projectile(Ship launcher, int speed, int timeTillDeath, int damage, string c)
    {
        _Name = "Projectile";
        _Launcher = launcher;
        LocationVector l = new LocationVector();
        l.x = launcher.Location.x;
        l.y = launcher.Location.y;
        SetLocation(l);
        _Vector = new VelocityVector();
        _Vector.x = launcher.CurrentSpeedVector.x;
        _Vector.y = launcher.CurrentSpeedVector.y;
        _Vector.AccelerateInDirection(speed, launcher.Angle());
        _TimeTillDeath = timeTillDeath;
        _ContactDamage = damage;
        _Class = c;
     }

    public override void Update(double deltaTime)
    {

        _TimeTillDeath-= (float)deltaTime;

        if (_TimeTillDeath <= 0)
        {
            Destroy();
        }

     Location.Advance(_Vector, deltaTime);

        DestroyableObject contact = CheckForContact();
 
        if (contact != null)
        {
            //Different teams
            if (contact.Team != _Launcher.Team && contact.Team != null)
            {
                contact.TakeDamage(_ContactDamage);

                Destroy();
            }
            
        }
    }

    private DestroyableObject CheckForContact()
    {
        Type[] types = new Type[1];
        types[0] = typeof(DestroyableObject);

        DestroyableObject phys = (DestroyableObject)UniverseController.GetClosestOfType(types, Location, this);
        if (phys == null)
        {

        }
        //TODO: change 10 to scale with contact size
        else if (LocationVector.Distance(phys.Location, this.Location) < 10)
        {
            /*if (phys is Celestial)
                if ((phys as Celestial).AttachedColony() != null)
                {
                    Universe.OutputText("Projectile hitting planet -> colony");
                    return (phys as Celestial).AttachedColony();
                }
                */
            return phys;
        }

        return null;
    }

}








