﻿
public abstract class Module : NamedObject {

    private int _Volume;
    private int _Mass;
    private int _EnergyUse;

    public Module(int volume, int mass)
    {
        _Volume = volume;
        _Mass = mass;
    }

    public int Mass
    {
        get { return _Mass; }
    }

    public abstract void OnUse(Ship ship);
    public virtual void Update(double deltaTime)
    {
    }

}
