﻿using Assets.Model.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Model
{
    public abstract class ModulerObject : DestroyableObject
    {
        protected Module[] _Modules;

        public Module[] GetModules()
        {
            return _Modules;
        }

        protected float _Mass
        {
            get
            {
                float mass = 0;

                foreach (Module m in _Modules)
                {
                    if (m != null)
                        mass += m.Mass;
                }

                return mass;
            }
        }

        public void SetMaxModules(int number)
        {
            _Modules = new Module[number];
        }

        public void SetModules(Module[] modules)
        {
            for (int i = 0; i < modules.Length; i++)
            {
                _Modules[i] = modules[i];
            }
        }

        public override int ArmourPoints()
        {
            int armourFromModules = 0;

            foreach (Module m in _Modules)
            {
                if (m is Armour)
                {
                    armourFromModules = (m as Armour).ArmourPoints;
                }
            }

            return armourFromModules + _ArmourPoints;
        }
        public override int ArmourPointsMax()
        {
            int armourFromModules = 0;

            foreach (Module m in _Modules)
            {
                if (m is Armour)
                {
                    armourFromModules = (m as Armour).ArmourPointsMax;
                }
            }

            return armourFromModules + _ArmourPoints;
        }

        public override int ShieldPoints()
        {
            int shieldFromModules = 0;

            foreach (Module m in _Modules)
            {
                if (m is Shield)
                {
                    shieldFromModules = (m as Shield).ShieldPoints;
                }
            }

            return shieldFromModules + _ShieldPoints;
        }
        public override int ShieldPointsMax()
        {
            int shieldFromModules = 0;

            foreach (Module m in _Modules)
            {
                if (m is Shield)
                {
                    shieldFromModules = (m as Shield).ShieldPointsMax;
                }
            }

            return shieldFromModules + _ShieldPoints;
        }

        protected override int DamageShields(int dmg)
        {
            foreach (Module m in _Modules)
            {
                if (m is Shield)
                {
                    dmg = (m as Shield).DamageShield(dmg);
                }
            }

            return dmg;
        }

        protected override int DamageArmour(int dmg)
        {
            foreach (Module m in _Modules)
            {
                if (m is Armour)
                {
                    dmg = (m as Armour).DamageArmour(dmg);
                }
            }

            return dmg;
        }

        

    }

}
