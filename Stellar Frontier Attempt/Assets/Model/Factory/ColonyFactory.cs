﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Model
{
    class ColonyFactory
    {
        public static Colony MakeColony(Team team, int startPop, Celestial celestial)
        {
            Colony colony = new Colony(team, startPop);
            colony.SetClass("Colony");
            colony.SetMaxModules(3);
            Module[] defaultModules = new Module[3];
            defaultModules[0] = ModuleFactory.MakeWeapon();
            defaultModules[1] = ModuleFactory.MakeArmour();
            defaultModules[2] = ModuleFactory.MakeShield();
            colony.SetModules(defaultModules);

            if (celestial != null)
                colony.AttachTo(celestial);

            return colony;
        }

        public static Colony MakeColony(Team team, int startPop)
        {
            return MakeColony(team, startPop, null);
        }
    }
}
