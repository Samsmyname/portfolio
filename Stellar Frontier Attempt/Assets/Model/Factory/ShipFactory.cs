﻿using UnityEngine;
using System.Collections;

namespace Assets.Model {

    public static class ShipFactory {

        public static Ship MakeDreadnaught(string name, Pilot pilot)
        {
            Ship ship = new Ship();
            ship.SetClass("Dreadnaught");
            ship.SetPilot(pilot);
            ship.SetMaxModules(8);
            Module[] defaultModules = new Module[4];
            defaultModules[0] = ModuleFactory.MakeWeapon();
            defaultModules[1] = ModuleFactory.MakeArmour();
            defaultModules[2] = ModuleFactory.MakeShield();
            defaultModules[3] = ModuleFactory.MakeEngine();
            ship.SetModules(defaultModules);
            ship.NameObject(name);
            LocationVector l = new LocationVector();
            l.x = pilot.Team.HomePlanet().Location.x;
            l.y = pilot.Team.HomePlanet().Location.y;
            ship.SetLocation(l);

            return ship;
        }
    }
}