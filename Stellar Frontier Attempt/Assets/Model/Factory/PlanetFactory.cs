﻿

public static class PlanetFactory {

    public static Celestial MakePlanet(string name, int defaultOrbit, Celestial parent, int parentOrbitDistance, float angle)
    {
        Celestial planet = new Celestial();
        if (parent != null)
            planet.OrbitInfo.MakeOrbit(parent, parentOrbitDistance, angle);
        planet.NameObject(name);
        planet.SetDefaultOrbitDistance(defaultOrbit);

        return planet;
    }
    public static Celestial MakePlanet(string name, int defaultOrbit, Celestial parent, int parentOrbitDistance)
    {
        Celestial planet = MakePlanet(name, defaultOrbit, parent, parentOrbitDistance, 0);
        return planet;
    }

    public static Celestial MakePlanet(string name, int defaultOrbit)
    {
        Celestial planet = MakePlanet(name, defaultOrbit, null, 0, 0);
        return planet;
    }

}
