
using Assets.Model.Modules;

namespace Assets.Model {

    public static class ModuleFactory
    {

        public static Module MakeWeapon()
        {
            Module weapon = new Weapon(100, 100, Weapon.Position.FrontMiddle, 30, 200, 10, 100, "Mass Driver");

            return weapon;
        }

        public static Module MakeShield()
        {
            Module shield = new Shield(100, 100, 200);

            return shield;
        }

        public static Module MakeArmour()
        {
            Module armour = new Armour(100, 100, 200);

            return armour;
        }

        public static Module MakeEngine()
        {
            Module engine = new Engine(100, 100, 100);

            return engine;
        }

    }
}