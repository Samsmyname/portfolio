﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Model
{
    public class VelocityVector : Vector
    {


        public VelocityVector()
        {
            x = 0;
            y = 0;
        }

        public void AccelerateInDirection(float amount, float direction)
        {
            AccelerateInDirection(amount, direction, 0);
        }

        public void AccelerateInDirection(float amount, float direction, float limit)
        {
            direction *= (float)(Math.PI / 180);

            float xComp = amount * (float)Math.Cos(direction);
            float yComp = amount * (float)Math.Sin(direction);



            if (limit == 0)
            {
                x += xComp;
                y += yComp;
            }
            else
            {
                lock (_Lock)
                {
                    x += xComp;
                    y += yComp;
                    Limit(limit);
                }
            }
        }

        public void Limit(float maxAmount)
        {
            if (Total() > maxAmount)
            {
                float modifier = maxAmount / Total();

                x *= modifier;
                y *= modifier;
            }
        }

        public float Total()
        {
            return (float)Math.Sqrt((x * x) + (y * y));
        }



    }
}
