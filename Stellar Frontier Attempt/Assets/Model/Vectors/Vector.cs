﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Model
{
    public class Vector
    {

        protected object _Lock = new object();
        private float[] data = new float[2];

        public float x
        {
            get { return data[0]; }
            set { data[0] = value; }
        }

        public float y
        {
            get { return data[1]; }
            set { data[1] = value; }
        }


        public static Vector Zero
        {
            get
            {
                Vector v = new Vector();
                v.x = 0;
                v.y = 0;

                return v;
            }
            
       
        }
        public static float DegreesToRadians(float degrees)
        {
            return degrees * (float)(Math.PI / 180);
        }

        public static float RadiansToDegrees(float radians)
        {
            return (float)(radians * (180 / Math.PI));
        }

        public static Vector VectorFromCoords(Vector v1, Vector v2)
        {
            Vector v = new Vector();
            v.x = v2.x - v1.x;
            v.y = v2.y - v1.y;

            return v;
        }

        public static Vector AngleAsVector(float angle)
        {
            Vector v = new Vector();
            v.x = (float)Math.Cos(Vector.DegreesToRadians(angle));
            v.y = (float)Math.Sin(Vector.DegreesToRadians(angle));

            return v;
        }

        public float Magnitude()
        {
            return (float)Math.Sqrt((x*x) + (y*y));
        }

        public static float DotProduct(Vector v1, Vector v2)
        {
            return ((v1.x * v2.x) + (v1.y * v2.y));
        }

        public static float AngleBetweenInDegrees(Vector v1, Vector v2)
        {
            float r = (DotProduct(v1, v2) / (v1.Magnitude() * v2.Magnitude()));
            return (float)Math.Acos(r);
        }

    }
}
