﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Model
{
    public class LocationVector : Vector
    {

        public LocationVector()
        {
        }

        public LocationVector(float x, float y)
        {
            this.x = x;
            this.y = y;
        }


        public static float Distance(LocationVector location, LocationVector location2)
        {
            return (float)Math.Sqrt(Math.Pow((location2.x - location.x), 2) + Math.Pow((location2.y - location.y), 2));
        }

        public void Advance(VelocityVector acceleration, double deltaTime)
        {
            x += (float)((acceleration.x / 100) * deltaTime);
            y += (float)((acceleration.y / 100) * deltaTime);
        }
    }
}
