﻿using UnityEngine;
using System.Collections;
using Assets.Model;

public class OrbitInformation {

    //relating to things that I orbit
    private Celestial _OrbitingBody;
    private float _OrbitAngle;
    private float _OrbitDistance;
    private LocationVector _Location;
    private GameObject _Go;

    public OrbitInformation(GameObject go)
    {
        _Go = go;
        _Location = UniverseController.DefaultLocation;
        
    }

    public void MakeOrbit(Celestial orbitParent, float radius, float angle)
    {
        _OrbitingBody = orbitParent;
        _OrbitAngle = angle;
        _OrbitDistance = radius;
        _Location = OrbitAngleToCoords();
    }
    public void MakeOrbit(Celestial orbitParent, float radius)
    {
        _OrbitingBody = orbitParent;
        _OrbitAngle = 0;
        _OrbitDistance = radius;
        _Location = OrbitAngleToCoords();
    }

    public void MakeOrbit(Celestial orbitParent)
    {
        _OrbitingBody = orbitParent;
        _OrbitAngle = 0;
        _OrbitDistance = _OrbitingBody.DefaultOrbitDistance;
        _Location = OrbitAngleToCoords();
    }

    public void BreakOrbit()
    {
        _OrbitingBody = null;
    }

    public Celestial GetOrbitingBody()
    {
        return _OrbitingBody;
    }

    public float OrbitAdjustment(float location)
    {

        float moveDistance = 0.005f;
        float radius = _OrbitDistance;

        float angle = moveDistance / radius;

        _OrbitAngle += angle;
        _Location = OrbitAngleToCoords();

        if (angle > 2)
            angle -= 2;

        return angle;
    }

    public LocationVector OrbitAngleToCoords()
    {
        float angle = _OrbitAngle;
        LocationVector origin = _OrbitingBody.OrbitInfo.Location;
        float radius = _OrbitDistance;

        LocationVector point = new LocationVector();
        point.x = origin.x + radius * Mathf.Cos(angle);
        point.y = origin.y + radius * Mathf.Sin(angle);

        return point;
    }

    public bool IsOrbiting
    {
        get
        {
            if (_OrbitingBody != null)
                return true;
            else
                return false;
        }
    }

    public LocationVector Location
    {
        get
        {
            return _Location;
        }
        set
        {
            _Location = value;
        }
    }
}
