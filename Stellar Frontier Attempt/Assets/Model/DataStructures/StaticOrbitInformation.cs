﻿using Assets.Model;
using System;

public class StaticOrbitInformation {

    //relating to things that I orbit
    private Celestial _OrbitingBody;
    private float _OrbitAngle;
    private float _OrbitDistance;
    private LocationVector _Location;

    public StaticOrbitInformation()
    {
        _Location = UniverseController.DefaultLocation;   
    }

    public void MakeOrbit(Celestial orbitParent, float radius, float angle)
    {
        _OrbitingBody = orbitParent;
        _OrbitAngle = angle;
        _OrbitDistance = radius;
        _Location = OrbitAngleToCoords();
    }
    public void MakeOrbit(Celestial orbitParent, float radius)
    {
        _OrbitingBody = orbitParent;
        _OrbitAngle = 0;
        _OrbitDistance = radius;
        _Location = OrbitAngleToCoords();
    }

    public void MakeOrbit(Celestial orbitParent)
    {
        _OrbitingBody = orbitParent;
        _OrbitAngle = 0;
        _OrbitDistance = _OrbitingBody.DefaultOrbitDistance;
        _Location = OrbitAngleToCoords();
    }

    public void BreakOrbit()
    {
        _OrbitingBody = null;
    }

    public Celestial GetOrbitingBody()
    {
        return _OrbitingBody;
    }

    public float OrbitAdjustment(double deltaTime)
    {

        float moveDistance = (float)(0.005f * deltaTime);
        float radius = _OrbitDistance;

        float angle = moveDistance / radius;

        _OrbitAngle += angle;
        _Location = OrbitAngleToCoords();


        if (angle > 2)
            angle -= 2;

        //Adjust angle when in orbit

        return angle;
    }

    public float Angle
    {
        get { return _OrbitAngle; }
    }

    public LocationVector OrbitAngleToCoords()
    {
        float angle = _OrbitAngle;
        LocationVector origin = _OrbitingBody.OrbitInfo.Location;
        float radius = _OrbitDistance;

        LocationVector point = new LocationVector();
        point.x = (float)(origin.x + radius * Math.Cos(angle));
        point.y = (float)(origin.y + radius * Math.Sin(angle));

        return point;
    }

    public bool IsOrbiting
    {
        get
        {
            if (_OrbitingBody != null)
                return true;
            else
                return false;
        }
    }

    public LocationVector Location
    {
        get
        {
            return _Location;
        }
        set
        {
            _Location = value;
        }
    }
}
