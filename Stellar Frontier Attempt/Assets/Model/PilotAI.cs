﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Model
{
    public class PilotAI : Pilot
    {

        private Ship _Target;

        public enum Priority
        {
            Attack,
            Defend,
            Colonise,
            Hunt,
            Guard
        }

        private Priority _Priority;

        public PilotAI(string name, Team team) : base(name, team, false)
        {
            _Priority = Priority.Attack;
        }

        public void SetPriority(Priority p)
        {
            _Priority = p;
        }

        public override void Update()
        {
            if (MyShip() != null)
                {
                switch (_Priority)
                {
                    case Priority.Defend:
                        Defend();
                        break;
                    case Priority.Attack:
                        Attack();
                        break;
                    case Priority.Colonise:
                        Colonise();
                        break;
                }
            }
        }

        public void Defend()
        {

        }

        public void Attack()
        {
            
            if (_Target == null)
                _Target = GetShipTarget();
            
            if (_Target != null)
            {
                bool aimGood = AimAtTarget();
                GetToGoodDistance(aimGood);

                if (aimGood)
                    MyShip().FireWeapons();
            }
            else
            {
                UniverseController.OutputText("Target still null");
            }

        }

        public void Colonise()
        {

        }

        private Ship GetShipTarget()
        {
            Type[] types = new Type[1];
            types[0] = typeof(Ship);

            return (Ship)UniverseController.GetClosestOfTypeAndTeam(types, Team, MyShip().Location, MyShip());
        }

        private bool AimAtTarget()
        {
            Vector myAngle = MyShip().AngleAsVector();
            Vector angleFromMeToThem = Vector.VectorFromCoords(MyShip().Location, _Target.Location);

            float angleFromIdeal = Vector.RadiansToDegrees(Vector.AngleBetweenInDegrees(myAngle, angleFromMeToThem));

            if (angleFromIdeal > 1)
            {
                Vector testAngleVector = new Vector() { x = Vector.AngleAsVector(MyShip().Angle() + 1).x, y = Vector.AngleAsVector(MyShip().Angle() + 1).y };
                float testAngle = Vector.RadiansToDegrees(Vector.AngleBetweenInDegrees(testAngleVector, angleFromMeToThem));

                if (testAngle < angleFromIdeal)
                    MyShip().RotateLeft();
                else
                    MyShip().RotateRight();

                return false;
            }
            else
            {
                return true;
            }
        }

        private void GetToGoodDistance(bool facingCorrectDirection)
        {

            if (facingCorrectDirection)
            {
                if (LocationVector.Distance(MyShip().Location, _Target.Location) > 20)
                {
                    MyShip().Accelerate();
                }
                else
                {
                    MyShip().Brake();
                }
            }
            else
            {
                MyShip().Brake();
            }
            
        }
    }
}
