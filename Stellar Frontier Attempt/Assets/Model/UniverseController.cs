﻿using System.Collections.Generic;
using System.Xml.Linq;
using System;
using Assets.Model;

public static class UniverseController {

    public static LocationVector DefaultLocation = new LocationVector() { x = 0, y = 0 };
    public static Pilot player;

    private static Object _Lock = new object();
    private static List<NamedObject> _NamedObjects = new List<NamedObject>();
    private static List<Team> _Teams = new List<Team>();
    private static List<Pilot> _Pilots = new List<Pilot>();
    private static List<IListener> _Listeners = new List<IListener>();
    private static DateTime timeAtLastTick;
    private static float _RefreshRate = 120f;
    private static bool run = true;
    private static string _ResourcesPath = "Assets/Model/Data/";

    public static Dictionary<string, string[]> Sprites;

    public static void Load()
    {
        LoadMap(_ResourcesPath + "solarsystem.xml");

        Team testTeam = new Team("Test Team", _Teams.Count);
        _Teams.Add(testTeam);
        _NamedObjects.Add(testTeam);
        SetPlayer(new Pilot("Samuel", testTeam, true));

        LoadSprites();
        LoadRanks();
}

    private static void LoadRanks()
    {
        //XDocument xmlMap = XDocument.Load("ranks.xml");
    }
    private static void LoadSprites()
    {
        XDocument xmlMap = XDocument.Load(_ResourcesPath + "sprites.xml");
        var sprites = xmlMap.Element("sprites").Elements();
        Sprites = new Dictionary<string, string[]>();

        foreach (var sprite in sprites)
        {
            int c = 0;
            foreach (var variation in sprite.Elements())
            {
                c++; 
            }
            string[] variations = new string[c];
            c = 0;
            foreach (var variation in sprite.Elements())
            {
                variations[c] = variation.Attribute("sprite").Value;
                OutputText("Loaded: " + sprite.Attribute("name").Value);
                c++;
            }
            Sprites.Add(sprite.Attribute("name").Value, variations);
        }

        OutputText("Sprites Loaded.");
    }
    private static void LoadMap(string mapName)
    {
        OutputText("Map load started.");

        XDocument xmlMap = XDocument.Load(mapName);

        var stars = xmlMap.Element("map").Element("universe").Elements();
        var xteams = xmlMap.Element("map").Element("teams").Elements();

        foreach (var star in stars)
        {
            Celestial createdStar = AddCelestial(PlanetFactory.MakePlanet(star.Attribute("name").Value, int.Parse(star.Attribute("dorbit").Value)));
            createdStar.SetLocation(new LocationVector { x = 0, y = 0 });
            createdStar.SetClass(star.Attribute("name").Value);

            var planets = star.Elements();
            foreach (var planet in planets)
            {
                Celestial createdPlanet = AddCelestial(PlanetFactory.MakePlanet(planet.Attribute("name").Value, int.Parse(planet.Attribute("dorbit").Value)));
                createdPlanet.OrbitInfo.MakeOrbit(createdStar, float.Parse(planet.Attribute("orbit").Value), float.Parse(planet.Attribute("angle").Value));
                createdPlanet.SetClass(planet.Attribute("name").Value);

                var moons = planet.Elements();
                foreach (var moon in moons)
                {
                    Celestial createdMoon = AddCelestial(PlanetFactory.MakePlanet(moon.Attribute("name").Value, int.Parse(moon.Attribute("dorbit").Value)));
                    createdMoon.OrbitInfo.MakeOrbit(createdPlanet, float.Parse(moon.Attribute("orbit").Value), float.Parse(moon.Attribute("angle").Value));
                    createdMoon.SetClass(moon.Attribute("name").Value);
                }
            }


        }


        foreach (var xteam in xteams)
        {
            Team t = new Team(xteam.Attribute("name").Value, _Teams.Count);
            _Teams.Add(t);
            _NamedObjects.Add(t);

        }

        int aiNumber = 1;

        foreach (var xteam in xteams)
        {
            Team team = (GetObjectByName(xteam.Attribute("name").Value) as Team);
            team.SetSpriteNumber(xteam.Attribute("sprites").Value);
            var xcolonies = xteam.Elements();

            foreach (var xcolony in xcolonies)
            {
                Colony c = LoadColony(team, xcolony, (UniverseController.GetObjectByName(xcolony.Attribute("name").Value) as Celestial));

                if (xcolony.Attribute("home") != null)
                {
                    if (xcolony.Attribute("home").Value == "true")
                    {
                        team.SetHome(c);
                    }
                }
            }

            for (int i = 0; i < 5; i++)
            {
                _Pilots.Add(new PilotAI("AI " + aiNumber, team));
                aiNumber++;
            }
        }
        OutputText("Map Loaded.");
    }

    public static void SetPlayer(Pilot p)
    {
        player = p;

        foreach (IListener listener in _Listeners)
        {
            listener.PlayerChangedTo(p);
        }

    }


    public static void Start()
    {
        timeAtLastTick = DateTime.Now;
        while (run)
        {
            Tick();
        }

        OutputText("Game Loop Ended");
    }

    private static Colony LoadColony(Team team, XElement XCelestial, Celestial celestial)
    {
                int startpop = Int32.Parse(XCelestial.Attribute("startpop").Value);
                OutputText("Team: " + team.Name() + " Pop: " + startpop);
                Colony c = ColonyFactory.MakeColony(team, startpop, celestial);
                AddNamedObject(c);
                celestial.PlaceColony(c);

                return c;

    }

private static void AddNamedObject(NamedObject n)
    {
        lock (_Lock)
        {
            _NamedObjects.Add(n);
        }
    }
    private static void AddPhysicalObject(PhysicalObject n)
    {
        AddNamedObject(n);
    }
    public static Celestial AddCelestial (Celestial c)
    {
        AddNamedObject(c);
        return c;
    }
    public static Projectile AddProjectile(Projectile p)
    {
        AddNamedObject(p);
        return p;
    }

    public static Ship AddShip (Ship s)
    {
        AddNamedObject(s);
        return s;
    }

    public static List<Celestial> Celestials
    {
        get
        {
            List<Celestial> celestialObjects = new List<Celestial>();
            foreach (NamedObject o in AllObjects)
            {
                if (o is Celestial)
                    celestialObjects.Add(o as Celestial);
            }

            return celestialObjects;
        }
    }
    
    public static List<NamedObject> AllObjects
    {
        get
        {
            lock (_Lock)
            {
                return _NamedObjects;
            }
        }
    }

    public static List<PhysicalObject> PhysicalObjects
    {
        get
        {
            List<PhysicalObject> physicalObjects = new List<PhysicalObject>();
            foreach (NamedObject o in AllObjects)
            {
                if (o is PhysicalObject || o.GetType().IsSubclassOf(typeof(PhysicalObject)))
                    physicalObjects.Add(o as PhysicalObject);
            }

            return physicalObjects;
        }
    }

    public static Celestial GetPlanetNumber(int i)
    {
        if (Celestials.Count >= i)
            return Celestials[i];
        else
        {
            //Debug.LogError("Tried to get a planet that doesnt exist.");
            return null;
        }
    }

    public static NamedObject GetObjectByName(string name)
    {
        foreach (NamedObject n in AllObjects)
            if (n.AreYou(name))
                return n;

        //Debug.LogError("Couldn't find named object " + name + ".");    
        return null;
    }

    public static NamedObject GetClosestOfType(Type[] types, LocationVector location, NamedObject exclusion)
    {
        return GetClosestOfTypeAndTeam(types, null, location, exclusion);
    }

    public static NamedObject GetClosestOfTypeAndTeam(Type[] types, Team teamIAmOn, LocationVector location, NamedObject exclusion)
    {

        NamedObject result = null;
        float closest = Single.PositiveInfinity;

        foreach (PhysicalObject o in PhysicalObjects)
        {
            foreach (Type type in types)
            {
                if (o.Team != teamIAmOn)
                {
                    if ((o.GetType() == type || o.GetType().IsSubclassOf(type)) && o != exclusion)
                    {
                        float d = LocationVector.Distance(location, o.Location);
                        if (d < closest)
                        {
                            result = o;
                            closest = LocationVector.Distance(location, o.Location);

                        }
                    }
                }
            }

        }

        return result;
    }


    public static NamedObject GetClosestOfType(Type type, LocationVector location)
    {
        Type[] types = new Type[1];
        types[0] = type;
        return GetClosestOfType(types, location, null);
    }
    public static int PlanetCount
    {
        get
        {
            return Celestials.Count;
        }
    }

    public static void AddListener(IListener listener)
    {
        _Listeners.Add(listener);
    }

    public static void OutputText(string text)
    {
        foreach (IListener listener in _Listeners)
        {
            listener.OutputText(text);
        }
    }

    private static void RemoveObject(NamedObject obj)
    {
        lock (_Lock)
        {
            _NamedObjects.Remove(obj);
        }

        foreach (IListener listener in _Listeners)
        {
            listener.OnObjectDestroyed(obj);
        }
    }

    public static void Tick()
    {
        DateTime now = DateTime.Now;
        TimeSpan t = now - timeAtLastTick;
        double deltaTime = t.TotalSeconds;


        if (deltaTime > (1f/_RefreshRate))
        {
            deltaTime *= 100;
            
            foreach (Team team in _Teams)
            {
                team.Update(deltaTime);
            }

            for (int x = 0; x < UniverseController.PhysicalObjects.Count; x++)
            {

                PhysicalObject phys = UniverseController.PhysicalObjects[x];

                if (phys is DestroyableObject)
                    if ((phys as DestroyableObject).Destroyed)
                        RemoveObject(phys);

                phys.Update(deltaTime);

            }

            timeAtLastTick = DateTime.Now;
        }
    }

    public static void Stop()
    {
        run = false;
    }
}
