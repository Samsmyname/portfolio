﻿using UnityEngine;
using System.Collections.Generic;
using Assets.Model;
using System.Threading;
using System;
using UnityEngine.UI;

public class UnityGameController : MonoBehaviour, IListener {

    private UIPanel _ActivePanel;
    //private int _FontSize = 12;
    public GameObject CombatButton;
    public GameObject CombatPanel;
    public GameObject ShipStatsPanel;

    public PhysicalObject Focus;
    private Camera _Camera;

    public Dictionary<NamedObject, GameObject> GameObjects;
    private List<NamedObject> _DestroyQueue;

    private Thread gameLoop;

    public Pilot Player;

    public enum UIPanel
    {
        Combat,
        Planets,
        Victory,
        Settings
    }


    // Use this for initialization
    void Start () {

        GameObject cameraGameObject = new GameObject("MainCamera");
        _Camera = cameraGameObject.AddComponent<Camera>();
        _Camera.orthographic = true;
        _Camera.orthographicSize = 80;
        _Camera.nearClipPlane = 0.3f;
        _Camera.enabled = true;

        _ActivePanel = UIPanel.Combat;
        Change(_ActivePanel);

        GameObjects = new Dictionary<NamedObject, GameObject>();
        _DestroyQueue = new List<NamedObject>();

        UniverseController.AddListener(this);
        UniverseController.Load();

        foreach (PhysicalObject o in UniverseController.PhysicalObjects)
        {
            GiveGameObject(o);
            
        }

        gameLoop = new Thread(new ThreadStart(UniverseController.Start));
        gameLoop.Start();
        
    }

    public void PlayerChangedTo(Pilot player)
    {
        Player = player;
    }

    public void OutputText(string text)
    {
        Debug.Log(text);
    }

    public void OnObjectDestroyed(NamedObject obj)
    {
        _DestroyQueue.Add(obj);
    }

    GameObject GiveGameObject(PhysicalObject o)
    {


        GameObject go = new GameObject();
        if (GameObjects.TryGetValue(o, out go))
        {
            // Game object already exists!
        }
        else
        {
            go = new GameObject();
            go.name = o.Name();
            string sprite;
            if (o.Team != null)
            {
                if (o.Team.ID < UniverseController.Sprites[o.Class].Length)
                {
                    OutputText(o.Class + " Team ID: " + o.Team.ID);
                    sprite = UniverseController.Sprites[o.Class][o.Team.ID];
                }
                else
                {
                    sprite = UniverseController.Sprites[o.Class][0];
                }

            }
            else
            {
                OutputText("null sprite class " + o.Name());
                sprite = UniverseController.Sprites[o.Class][0];
            }
            SpriteRenderer sr = go.AddComponent<SpriteRenderer>();
            sr.sprite = (Sprite)Resources.Load<Sprite>("Images/" + sprite);
            go.transform.SetParent(transform, true);

            if (o is Ship)
            {
                sr.sortingLayerName = "Ships";
            }
            if (o is Celestial)
            {
                sr.sortingOrder = 1;
            }
            if (o is Projectile)
            {
                sr.sortingOrder = 3;
            }
            if (o is Colony)
            {
                sr.sortingOrder = 2;
            }

            GameObjects.Add(o, go);
        }
        return go;
    }



	// Update is called once per frame
	void Update () {

        // Remove GameObjects from model objects that no longer exist
        CheckDestroyQueue();

        // Input
        if (Player.MyShip() == null)
            Focus = Player.Team.HomePlanet();
        else
            Focus = Player.MyShip();

        if (Input.GetKey("left"))
        {
            Player.MyShip().RotateLeft();
        }
        if (Input.GetKey("right"))
        {
            Player.MyShip().RotateRight();
        }
        if (Input.GetKey("up"))
        {
            Player.MyShip().Accelerate();
        }
        if (Input.GetKey("down"))
        {
            Player.MyShip().Reverse();
        }
        if (Input.GetKeyDown("o"))
        {
        }
        if (Input.GetKeyDown("space"))
        {
            Player.MyShip().FireWeapons();
        }

        if (Input.GetMouseButtonDown(0))
        {
            LeftMouseButton();
        }
        if (Input.GetMouseButtonDown(1))
        {
            RightMouseButton();
        }

        // Update GameObjects
        for (int x = UniverseController.PhysicalObjects.Count - 1; x >= 0 ; x--)
        {
            PhysicalObject physicalObject = UniverseController.PhysicalObjects[x];

            //Check a unity game object is attached
            GameObject go = CheckGameObjectAttached(physicalObject);

            // Adjust sprite positions/rotations to match model
            go.transform.rotation = Quaternion.Euler(0, 0, (physicalObject.Angle() - 90));
            go.transform.position = new Vector2(physicalObject.Location.x, physicalObject.Location.y);
            
        }


        

        // Update interface
        UpdateInterface();

    }

    void LateUpdate()
    {
        // Update camera position
        _Camera.transform.position = new Vector3(Focus.Location.x, Focus.Location.y, -20);
    }

    void OnApplicationQuit()
    {
        UniverseController.Stop();
    }

    private void CheckDestroyQueue()
    {
        foreach (NamedObject obj in _DestroyQueue)
        {

            GameObject go;
            GameObjects.TryGetValue(obj, out go);

            // from universecontroller
            GameObjects.Remove(obj);

            // from unity
            Destroy(go);
        }

        _DestroyQueue.Clear();
    }

    private GameObject CheckGameObjectAttached(PhysicalObject obj)
    {
        GameObject go;
        if (!GameObjects.TryGetValue(obj, out go))
        {
            go = GiveGameObject(obj);
        }
        return go;
    }

    private void LeftMouseButton()
    {
        LocationVector mouse = new LocationVector();
        Vector3 mousePosition = _Camera.ScreenToWorldPoint(Input.mousePosition);
        mouse.x = mousePosition.x;
        mouse.y = mousePosition.y;

        Type[] types = new Type[3];
        types[0] = typeof(Ship);
        types[1] = typeof(Celestial);
        types[2] = typeof(Colony);

        PhysicalObject result = (PhysicalObject)UniverseController.GetClosestOfType(types, mouse, null);

        GameObject go;
        GameObjects.TryGetValue(result, out go);
        Sprite sr = go.GetComponent<SpriteRenderer>().sprite;

        float spriteSize = sr.bounds.size.x;
        float distance = LocationVector.Distance(result.Location, mouse);

        if (distance < spriteSize)
        {
            if (result is Celestial)
            {
                if ((result as Celestial).AttachedColony() != null)
                {
                    Player.SetTarget((result as Celestial).AttachedColony());
                }
                else
                    Player.SetTarget(result);

            }
            else
            {
                Player.SetTarget(result);
            }
        }
    }

    private void RightMouseButton()
    {
        Player.SetTarget(null);
    }

    public void Change(UIPanel panel)
    {
        _ActivePanel = panel;

        switch (panel)
        {
            case UIPanel.Combat:
                {
                    CombatPanel.SetActive(true);
                }
                break;
            case UIPanel.Planets:
                {

                }
                break;
            case UIPanel.Settings:
                {

                }
                break;
            case UIPanel.Victory:
                {

                }
                break;
        }
    }

    private void UpdateInterface()
    {
        Text[] combatText = CombatPanel.GetComponentsInChildren<Text>();

        Text panelName = combatText[0];
        Text targetName = combatText[1];
        Text hullPoint = combatText[2];
        Text shieldPoints = combatText[3];
        Text armourPoints = combatText[4];
        Text teamName = combatText[5];
        Text population = combatText[6];
        Text growth = combatText[7];
        Text something = combatText[8];

        panelName.text = "Target Information";
        targetName.text = "";
        hullPoint.text = "";
        shieldPoints.text = "";
        armourPoints.text = "";
        teamName.text = "";
        population.text = "";
        growth.text = "";
        something.text = "";

        if (UniverseController.player.HasTarget())
        {
            PhysicalObject tar = UniverseController.player.Target;

            panelName.text = "Target Information";
            targetName.text = UniverseController.player.Target.Name();

            if (tar is ModulerObject)

                if (tar is Colony)
                {
                    population.text = "Pop: " + (tar as Colony).Population;
                    growth.text = "Growth: " + (tar as Colony).Growth;
                    something.text = "";
                }

            if (tar.GetType().IsSubclassOf(typeof(DestroyableObject)))
            {
                DestroyableObject tarD = (tar as DestroyableObject);
                hullPoint.text = "Hull: " + tarD.HullPoints() + " / " + tarD.HullPointsMax();
                shieldPoints.text = "Shield: " + tarD.ShieldPoints() + " / " + tarD.ShieldPointsMax();
                armourPoints.text = "Armour: " + tarD.ArmourPoints() + " / " + tarD.ArmourPointsMax();

                if (tar is Colony)
                {

                }

            }

            if (tar.Team != null)
                teamName.text = "Team: " + tar.Team.Name();
            else
                teamName.text = "Team: Neutral";

        }
        else
        {
            targetName.text = "No Target";

        }

        Ship ship = UniverseController.player.MyShip();

        if (ship != null)
        {
            Text[] shipText = ShipStatsPanel.GetComponentsInChildren<Text>();



            shipText[0].text = ship.HullPoints().ToString();
            shipText[1].text = ship.ArmourPoints().ToString();
            shipText[2].text = ship.ShieldPoints().ToString();
        }
    }

    void OnGUI()
    {
        float x = UniverseController.player.MyShip().Location.x;
        float y = UniverseController.player.MyShip().Location.y;
        Vector3 v3 = _Camera.WorldToScreenPoint(new Vector3(x, y, 0));
        Rect r = new Rect(v3.x, v3.y, 20, 10);
        GUI.Label(r, "texttextextext");
    }
}
