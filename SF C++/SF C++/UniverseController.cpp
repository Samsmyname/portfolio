#include "UniverseController.h"
#include "tinyxml2.h"
#include "PlanetFactory.h"
#include "ColonyFactory.h"
#include "Celestial.h"
#include "vec2.h"
#include "Team.h"
#include "Colony.h"
#include "PilotAI.h"
#include "SDL.h"
#include "SDL_image.h"
#include "Ship.h"

#include <iostream>

using namespace tinyxml2;
using namespace std;

vector<Team*> UniverseController::_Teams;
vector<NamedObject *> UniverseController::_NamedObjects;
vector<PhysicalObject *> UniverseController::_PhysicalObjects;
vector<Celestial *> UniverseController::_CelestialObjects;
vector<Pilot *> UniverseController::_Pilots;
vector<string> UniverseController::_Resources;
chrono::system_clock::time_point UniverseController::timeAtLastTick;
Pilot UniverseController::player;

bool UniverseController::UpKey;
bool UniverseController::DownKey;
bool UniverseController::LeftKey;
bool UniverseController::RighKey;
bool UniverseController::SpaceKey;

vec2<float> UniverseController::DefaultLocation()
{
	return vec2<float>();
}

void UniverseController::Load()
{
	LoadMap("");
}

void UniverseController::AddPhysicalObject(PhysicalObject * p)
{
	_PhysicalObjects.push_back(p);
}

Celestial * UniverseController::AddCelestial(Celestial * c)
{
	AddNamedObject(c);
	AddPhysicalObject(c);
	_CelestialObjects.push_back(c);
	return c;
}

vector<PhysicalObject*> UniverseController::PhysicalObjects()
{
	return _PhysicalObjects;
}

Celestial * UniverseController::GetCelestialByName(string name)
{
	for (Celestial * c : _CelestialObjects)
	{
		if (c->Name() == name)
			return c;
	}
}


void UniverseController::Tick()
{
	if (UpKey)
		player.MyShip()->Accelerate();
	if (DownKey)
		player.MyShip()->Reverse();
	if (LeftKey)
		player.MyShip()->RotateLeft();
	if (RighKey)
		player.MyShip()->RotateRight();
	if (SpaceKey)
		player.MyShip()->FireWeapons();

	chrono::system_clock::time_point now = chrono::system_clock::now();
	chrono::system_clock::duration t = now - timeAtLastTick;
	double deltaTime = (chrono::duration_cast<chrono::milliseconds>(t).count());
	deltaTime *= 1;
	//cout << "Tick " << deltaTime << endl;

	if (deltaTime > 0)
	{
		timeAtLastTick = now;

		for (PhysicalObject * p : PhysicalObjects())
		{
			p->Update(deltaTime);
			//cout << p->Name() << endl;
		}

	}

	
}

void UniverseController::Start()
{
	timeAtLastTick = chrono::system_clock::now();

}

void UniverseController::LoadMap(string mapName)
{
	XMLDocument xmlMap;
	xmlMap.LoadFile("solarsystem.xml");
	auto map = xmlMap.FirstChild();
	for (XMLElement * star = map->FirstChildElement("universe")->FirstChildElement(); star!=NULL; star = star->NextSiblingElement())
	{
		Celestial * createdStar = AddCelestial(PlanetFactory::MakePlanet(star->Attribute("name"), star->IntAttribute("dorbit")));
		createdStar->SetLocation(vec2<float>{ 0, 0 });
		createdStar->SetClass(star->Attribute("name"));
		createdStar->Rect.w = star->IntAttribute("size");
		createdStar->Rect.h = star->IntAttribute("size");

		
		string spritename = "Resources/Images/";
		spritename += star->Attribute("sprite");
		spritename += ".png";
		createdStar->SpriteString = spritename;
		_Resources.push_back(spritename);

		const char * name;
		name = star->Attribute("name");
		cout << name << endl;

		for (XMLElement * planet = star->FirstChildElement(); planet != NULL; planet = planet->NextSiblingElement())
		{

			Celestial * createdPlanet = AddCelestial(PlanetFactory::MakePlanet(planet->Attribute("name"), planet->IntAttribute("dorbit")));
			createdPlanet->MakeOrbit(*createdStar, planet->IntAttribute("orbit"), planet->FloatAttribute("angle"));
			createdPlanet->SetClass(planet->Attribute("name"));

			spritename = "Resources/Images/";
			spritename += planet->Attribute("sprite");
			spritename += ".png";
			createdPlanet->SpriteString = spritename;
			_Resources.push_back(spritename);

			createdPlanet->Rect.w = planet->IntAttribute("size");
			createdPlanet->Rect.h = planet->IntAttribute("size");

			name = planet->Attribute("name");
			cout << name << endl;

			for (XMLElement * moon = planet->FirstChildElement(); moon != NULL; moon = moon->NextSiblingElement())
			{

				Celestial * createdMoon = AddCelestial(PlanetFactory::MakePlanet(moon->Attribute("name"), moon->IntAttribute("dorbit")));
				createdMoon->MakeOrbit(*createdPlanet, moon->IntAttribute("orbit"), moon->FloatAttribute("angle"));
				createdMoon->SetClass(moon->Attribute("name"));

				spritename = "Resources/Images/";
				spritename += moon->Attribute("sprite");
				spritename += ".png";
				createdMoon->SpriteString = spritename;
				_Resources.push_back(spritename);

				createdMoon->Rect.w = moon->IntAttribute("size");
				createdMoon->Rect.h = moon->IntAttribute("size");

				name = moon->Attribute("name");
				cout << name << endl;


			}

		}
	}

	string spritename = "Resources/Images/";
	spritename += "SpaceShipRed";
	spritename += ".png";
	_Resources.push_back(spritename);

	string spritename2 = "Resources/Images/ColonySprite.png";
	_Resources.push_back(spritename2);

	string spritename3 = "Resources/Images/massdriver.png";
	_Resources.push_back(spritename3);
		
	for (XMLElement * xteam = map->FirstChildElement("teams")->FirstChildElement(); xteam != NULL; xteam = xteam->NextSiblingElement())
	{
		string name = xteam->Attribute("name");
		Team * t = new Team(name, _Teams.size());
		_Teams.push_back(t);
		_NamedObjects.push_back(t);

		cout << name << endl;

		for (XMLElement * xcolony = xteam->FirstChildElement(); xcolony != NULL; xcolony = xcolony->NextSiblingElement())
		{
			Colony * c = LoadColony(t, xcolony->IntAttribute("startpop"), UniverseController::GetCelestialByName(xcolony->Attribute("name")));
			c->SpriteString = spritename2;
			if (xcolony->BoolAttribute("home") == true)
			{
				t->SetHome(c);
			}
		}

		for (int i = 0; i < 5; i++)
		{
			PilotAI * pi = new PilotAI(t->Name() + " AI " + to_string(i), t);
			pi->MyTeam()->Sprites["ship"] = spritename;
			pi->SpawnShip();
			_Pilots.push_back(pi);
			cout << t->Name() + " AI " + to_string(i) << endl;
		}

	}

	player = Pilot("Samuel", _Teams[0], true);
	player.MyTeam()->Sprites["ship"] = spritename;
	player.SpawnShip();
	_Pilots.push_back(&player);



}

Colony * UniverseController::LoadColony(Team * team, int startpop, Celestial * celestial)
{
	Colony * c = ColonyFactory::MakeColony(team, startpop, celestial);
	AddNamedObject(c);
	AddPhysicalObject(c);
	return c;
}

void UniverseController::AddNamedObject(NamedObject * n)
{
	_NamedObjects.push_back(n);
}

