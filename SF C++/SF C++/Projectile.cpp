#include "Projectile.h"
#include "Ship.h"
#include "UniverseController.h"

DestroyableObject * Projectile::CheckForContact()
{
	return nullptr;
}

Projectile::Projectile(Ship * launcher, vec2<float> speed, int timeTillDeath, int damage, string c)
{
	_Launcher = launcher;
	_Vector = speed;
	_TimeTillDeath = timeTillDeath;
	_ContactDamage = damage;
	_Name = c;
	SetLocation(launcher->GetLocation());
	CurrentHeading = launcher->CurrentHeading;
	CurrentSpeed = speed;
	Rect.x = 5;
	Rect.y = 5;
}

void Projectile::Update(double deltaTime)
{
	auto temp = this;
	auto temp2 = _Launcher;
	vec2<float> change = (CurrentSpeed * deltaTime);
	SetLocation(GetLocation() += change);

	_TimeTillDeath -= deltaTime;

	if (_TimeTillDeath <= 0)
	{
		_Destroyed = true;
		typedef UniverseController UC;
		
		auto b = UC::_PhysicalObjects.begin();
		auto e = UC::_PhysicalObjects.end();

		UC::_PhysicalObjects.erase(find(b, e, this), e);
		delete this;
	}
}
