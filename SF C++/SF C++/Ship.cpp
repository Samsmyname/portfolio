#include "Ship.h"
#include "Pilot.h"
#include "Team.h"
#include "Module.h"
#include "Engine.h"
#include "Shield.h"
#include "Armour.h"

Ship::Ship()
{
	_Acceleration = 1;
	_MaxSpeed = 50;
	_RotationalSpeed = 0.05;
	//_CurrentHeading = new vec2<float>{ 0, 0 };
	//_CurrentSpeed = new vec2<float>{ 0, 0 };
	//_Location = new vec2<float>{ 100, 100 };
}

Team * Ship::MyTeam()
{
	return _Pilot->MyTeam();
}

void Ship::SetPilot(Pilot * P)
{
	_Pilot = P;
}

void Ship::SetStartingTeam(Team * t)
{
	_Team = t;
}

void Ship::AIMove()
{
}

void Ship::RotateLeft()
{
	CurrentHeading.rotate(-_RotationalSpeed);
}

void Ship::RotateRight()
{
	CurrentHeading.rotate(_RotationalSpeed);
}

float Ship::GetAcceleration()
{
	float acc = _Acceleration;
	for (Module * m : _Modules)
	{

		if (m->GetModuleType() == ModuleType::Engine)
		{
			Engine * e = dynamic_cast<Engine *>(m);
			acc += e->Power();
		}
	}

	acc /= Mass();
	acc /= 500;
	return acc;
}

void Ship::Accelerate()
{
	auto t = CurrentHeading.normalize() * GetAcceleration();
	CurrentSpeed = CurrentSpeed + t;
}

void Ship::Reverse()
{
	auto t = CurrentHeading.normalize() * GetAcceleration() *-1;
	CurrentSpeed = CurrentSpeed + t;
}

void Ship::Brake()
{
	//vec2<float> change = (*_CurrentHeading * 0.05f);
	//*_CurrentSpeed -= (change);
	//_CurrentSpeed.Limit(Math.Abs(_CurrentSpeed.Magnitude() - 0.05f));
}

void Ship::FireWeapons()
{
	for (Module * m : _Modules)
	{
		if (m->GetModuleType() == ModuleType::Weapon)
		{
			m->OnUse(this);
		}
	}
}

void Ship::Update(double deltaTime)
{
	vec2<float> change = (CurrentSpeed * deltaTime);
	SetLocation(GetLocation() += change);

	for (Module * m : _Modules)
	{
		m->Update(deltaTime);
	}
}
