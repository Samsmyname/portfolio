#pragma once

#include "PhysicalObject.h"

class DestroyableObject : public PhysicalObject
{
protected:
	int _HullPoints;
	int _ArmourPoints;
	int _ShieldPoints;
	int _HullPointsMax;
	int _ArmourPointsMax;
	int _ShieldPointsMax;
	bool _Destroyed;

	virtual int DamageShields(int dmg);
	virtual int DamageArmour(int dmg);
	void DamageHull(int dmg);

	virtual void Destroy();

public:
	DestroyableObject();
	void TakeDamage(int dmg);
	int HullPoints();
	int HullPointsMax();
	virtual int ArmourPoints();
	virtual int ArmourPointsMax();
	virtual int ShieldPoints();
	virtual int ShieldPointsMax();

	bool Destroyed();

};