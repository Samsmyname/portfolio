#pragma once

//#include "Module.h"
#include "DestroyableObject.h"
//#include "Shield.h"
//#include "Armour.h"

#include <vector>

class Module;

using namespace std;

class ModulerObject : public  DestroyableObject
{
protected:
	vector<Module *> _Modules;
	float Mass();

	int DamageShields(int dmg) override;
	int DamageArmour(int dmg) override;

public:
	vector<Module *> GetModules();
	void SetMaxModules(int number);
	void SetModules(vector<Module *> modules);
	int ArmourPoints() override;
	int ArmourPointsMax() override;
	int ShieldPoints() override;
	int ShieldPointsMax() override;
};