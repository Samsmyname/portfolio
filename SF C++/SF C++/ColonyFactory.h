#pragma once

//#include "Colony.h"

class Team;
class Colony;
class Celestial;

static class ColonyFactory
{
public:
	static Colony * MakeColony(Team * team, int startPop, Celestial * celestial);
	static Colony * MakeColony(Team * team, int startPop);
};