#pragma once

//#include "Ship.h"
#include <string>

using namespace std;

class Ship;
class Pilot;

static class ShipFactory
{
public:
	static Ship * MakeDreadnaught(string name, Pilot * pilot);
};