#include "PhysicalObject.h"
#include "Team.h"

PhysicalObject::PhysicalObject()
{
	_Angle = 0;
	Rect.x = 0;
	Rect.y = 0;
	Rect.h = 50;
	Rect.w = 50;

}

string PhysicalObject::Class()
{
	return _Class;
}

void PhysicalObject::SetClass(string c)
{
	_Class = c;
}

Team * PhysicalObject::MyTeam()
{
	return _Team;
}

void PhysicalObject::SetLocation(vec2<float> location)
{
	_Location = location;
}

vec2<float> PhysicalObject::GetLocation()
{
	return _Location;
}
