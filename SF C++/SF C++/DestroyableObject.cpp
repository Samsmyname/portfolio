#include "DestroyableObject.h"

int DestroyableObject::DamageShields(int dmg)
{
	if (_ShieldPoints > dmg)
	{
		_ShieldPoints -= dmg;
		return 0;
	}
	else
	{
		dmg -= _ShieldPoints;
		_ShieldPoints = 0;
	}

	return dmg;
}

int DestroyableObject::DamageArmour(int dmg)
{
	if (_ArmourPoints > dmg)
	{
		_ArmourPoints -= dmg;
		return 0;
	}
	else
	{
		dmg -= _ArmourPoints;
		_ArmourPoints = 0;
	}

	return dmg;
}

void DestroyableObject::DamageHull(int dmg)
{
	if (_HullPoints > dmg)
	{
		_HullPoints -= dmg;
	}
	else
	{
		_HullPoints = 0;
		Destroy();
	}
}

void DestroyableObject::Destroy()
{
	_Destroyed = true;
}

DestroyableObject::DestroyableObject()
{
	_HullPointsMax = 100;
	_HullPoints = _HullPointsMax;
	_Destroyed = false;
}

void DestroyableObject::TakeDamage(int dmg)
{
	dmg = DamageShields(dmg);
	dmg = DamageArmour(dmg);
	DamageHull(dmg);
}

int DestroyableObject::HullPoints()
{
	return _HullPoints;
}

int DestroyableObject::HullPointsMax()
{
	return _HullPointsMax;
}

int DestroyableObject::ArmourPoints()
{
	return _ArmourPoints;
}

int DestroyableObject::ArmourPointsMax()
{
	return _ArmourPointsMax;
}

int DestroyableObject::ShieldPoints()
{
	return _ShieldPoints;
}

int DestroyableObject::ShieldPointsMax()
{
	return _ShieldPointsMax;
}

bool DestroyableObject::Destroyed()
{
	return _Destroyed;
}
