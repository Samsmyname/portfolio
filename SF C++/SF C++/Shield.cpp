#include "Shield.h"

Shield::Shield(int volume, int mass, int maxPoints) : Module(volume, mass)
{
	_ShieldPointsMax = maxPoints;
	_ShieldPoints = _ShieldPointsMax;
}

int Shield::ShieldPoints()
{
	return _ShieldPoints;
}

int Shield::ShieldPointsMax()
{
	return _ShieldPointsMax;
}

int Shield::DamageShield(int dmg)
{
	if (_ShieldPoints > dmg)
	{
		_ShieldPoints -= dmg;
		return 0;
	}
	else
	{
		dmg -= _ShieldPoints;
		_ShieldPoints = 0;
		return dmg;
	}
}

void Shield::OnUse(Ship * ship)
{
}

void Shield::Update(double deltaTime)
{
}
