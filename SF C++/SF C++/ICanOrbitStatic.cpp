#include "ICanOrbitStatic.h"
#include "UniverseController.h"
#include "Celestial.h"

#include <cmath>

ICanOrbitStatic::ICanOrbitStatic()
{
	_Location = UniverseController::DefaultLocation();
}

void ICanOrbitStatic::MakeOrbit(Celestial & orbitParent, float radius, float angle)
{
	_OrbitingBody = &orbitParent;
	_OrbitAngle = angle;
	_OrbitDistance = radius;
	_Location = OrbitAngleToCoords();
}

void ICanOrbitStatic::MakeOrbit(Celestial & orbitParent, float radius)
{
	_OrbitingBody = &orbitParent;
	_OrbitAngle = 0;
	_OrbitDistance = radius;
	_Location = OrbitAngleToCoords();
}

void ICanOrbitStatic::MakeOrbit(Celestial & orbitParent)
{
	_OrbitingBody = &orbitParent;
	_OrbitAngle = 0;
	_OrbitDistance = _OrbitingBody->DefaultOrbitDistance();
	_Location = OrbitAngleToCoords();
}

void ICanOrbitStatic::BreakOrbit()
{

	//TODO check
	_OrbitingBody = nullptr;
}

Celestial * ICanOrbitStatic::GetOrbitingBody()
{
	return _OrbitingBody;
}

float ICanOrbitStatic::OrbitAdjustment(double deltaTime)
{
	float moveDistance = (float)(0.005f * deltaTime);
	float radius = _OrbitDistance;

	float angle = moveDistance / radius;

	
	_Location = OrbitAngleToCoords();

	if (angle > 4)
	{
		angle = 1;
		cout << "hmmm";
	}
	while(angle > 2)
	
		angle -=2;

	_OrbitAngle += angle;

	//Adjust angle when in orbit
	return angle;
}

float ICanOrbitStatic::Angle()
{
	return _OrbitAngle;
}

vec2<float> ICanOrbitStatic::OrbitAngleToCoords()
{
	float angle = _OrbitAngle;
	vec2<float> origin = _OrbitingBody->Location();
	float radius = _OrbitDistance;

	vec2<float> point;
	point.x = (float)(origin.x + radius * cos(angle));
	point.y = (float)(origin.y + radius * sin(angle));

	return point;
}

bool ICanOrbitStatic::IsOrbiting()
{
	if (_OrbitingBody != nullptr)
		return true;
	else
		return false;
}

vec2<float> ICanOrbitStatic::Location()
{
	return _Location;
}

/*void ICanOrbitStatic::SetLocation(vec2<float> location)
{
	_Location = location;
}*/
