#include "Armour.h"

Armour::Armour(int volume, int mass, int maxPoints) : Module(volume, mass)
{
	_ArmourPointsMax = maxPoints;
	_ArmourPoints = _ArmourPointsMax;
}

int Armour::ArmourPoints()
{
	return _ArmourPoints;
}

int Armour::ArmourPointsMax()
{
	return _ArmourPointsMax;
}

int Armour::DamageArmour(int dmg)
{
	if (_ArmourPoints > dmg)
	{
		_ArmourPoints -= dmg;
		return 0;
	}
	else
	{
		dmg -= _ArmourPoints;
		_ArmourPoints = 0;
		return dmg;
	}
}

void Armour::OnUse(Ship * ship)
{
}

void Armour::SetArmourValue(int value)
{
	_ArmourPoints = value;
}

void Armour::Update(double deltaTime)
{
}
