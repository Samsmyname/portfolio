#pragma once

#include "PhysicalObject.h"
#include "ICanOrbitStatic.h"
//#include "Colony.h"

class Colony;

class Celestial : public PhysicalObject, public ICanOrbitStatic
{
private:
	Colony * _Colony;

protected:
	float OrbitMeDistance;

public:
	Celestial();
	void Update(double deltaTime) override;
	void SetDefaultOrbitDistance(int distance);
	Colony * AttachedColony();
	void RemoveColony();
	void PlaceColony(Colony * colony);
	float DefaultOrbitDistance();
	vec2<float> GetLocation() override;
};