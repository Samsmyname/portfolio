#pragma once

#include "Module.h"

class Shield : public Module
{
private:
	int _ShieldPoints;
	int _ShieldPointsMax;

public:
	Shield(int volume, int mass, int maxPoints);
	int ShieldPoints();
	int ShieldPointsMax();
	int DamageShield(int dmg);
	void OnUse(Ship * ship) override;
	void Update(double deltaTime) override;
};