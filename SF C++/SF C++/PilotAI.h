#pragma once

#include "Pilot.h"

class Ship;

class PilotAI : public Pilot
{
private:
	Ship * _Target;
public:
	PilotAI(string name, Team * team);
};