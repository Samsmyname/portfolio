#pragma once

#include "Module.h"

class Armour : public Module
{
private:
	int _ArmourPoints;
	int _ArmourPointsMax;

public:
	Armour(int volume, int mass, int maxPoints);
	int ArmourPoints();
	int ArmourPointsMax();
	int DamageArmour(int dmg);
	void OnUse(Ship * ship) override;
	void SetArmourValue(int value);
	void Update(double deltaTime) override;
};