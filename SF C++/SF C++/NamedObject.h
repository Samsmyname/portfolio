#pragma once

#include <iostream>
#include <string>

using namespace std;

class NamedObject
{
protected:
	string  _Name;

public:
	NamedObject(string name);
	NamedObject();
	virtual void NameObject(string name);
	bool AreYou(string name);
	virtual string Name();
};