#pragma once

#include "ModulerObject.h"

//class Team;

class Celestial;

class Colony : public ModulerObject
{
private:
	Celestial * _AttachedTo;
	float _Population;
	float _Growth;
	float _StartPop;

public:
	Colony(Team * team, int startpop);
	void AttachTo(Celestial * c);
	float Population();
	float Growth();
	string Name() override;
	void Destroy() override;
	void Update(double deltaTime) override;

};