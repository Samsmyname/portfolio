#pragma once

#include "Module.h"

class Engine : public Module
{
private:
	int _Power;

public:
	Engine(int volume, int mass, int power);
	int Power();
	void OnUse(Ship * ship) override;
	void Update(double deltaTime) override;
};