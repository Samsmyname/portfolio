#pragma once

#include <vector>
#include <SDL.h>
#include <unordered_map>

using namespace std;

class View
{
private:
	SDL_Window * window;
	SDL_Renderer* renderer;

	unordered_map<string, SDL_Texture *> sprites;

	float cameraX;
	float cameraY;
	float cameraWidth;
	float cameraHeight;


public:
	void Update();
	void Setup();
	void Shutdown();
	void MoveCamera(int x, int y);
	float Zoom;
};