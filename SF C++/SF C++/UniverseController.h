#pragma once

#include "vec2.h"
//#include "Pilot.h"
//#include "NamedObject.h"
//#include "Team.h"
//#include "Celestial.h"
//#include "Projectile.h"

#include <unordered_map>
#include <chrono>

class Pilot;
class NamedObject;
class Team;
class Celestial;
class Projectile;
class PhysicalObject;
class Ship;
class Colony;


using namespace std;
//using namespace System;

class UniverseController
{
public:
	static vec2<float> DefaultLocation();
	static Pilot player;

	static vector<NamedObject *> _NamedObjects;
	static vector<PhysicalObject *> _PhysicalObjects;
	static vector<Celestial *> _CelestialObjects;
	static vector<Team *> _Teams;
	static vector<Pilot *> _Pilots;

	static chrono::system_clock::time_point timeAtLastTick;
	static float _RefreshRate;
	static bool run;
	static string _ResourcesPath;
	static vector<string> _Resources;

	static bool UpKey;
	static bool DownKey;
	static bool LeftKey;
	static bool RighKey;
	static bool SpaceKey;
	
	static unordered_map<string, vector<string>> Sprites;

	static void Load();

	static void AddPhysicalObject(PhysicalObject * p);
	static Celestial * AddCelestial(Celestial * c);
	//static Projectile * AddProjectile(Projectile * p);
	//static Ship * AddShip(Ship * s);
	//static vector<Celestial> Celestials;
	//static vector<NamedObject> AllObjects();
	static vector <PhysicalObject *> PhysicalObjects();
	//static Celestial * GetPlanetNumber(int i);
	//static NamedObject GetObjectByName(string name);
	static Celestial * GetCelestialByName(string name);
	//static NamedObject GetClosestOfType(vector<Type> types, vec2<float> location, NamedObject * exclusion);
	//static NamedObject GetClosestOfTypeAndTeam(vector<Type> types, Team * teamIAmOn, vec2<float> location, NamedObject * exclusion);
	//static NamedObject GetClosestOfType(Type type, vec2<float> Location);
	//static int PlanetCount();
	static void Tick();
	//static void Stop();
	static void Start();

private:
	//static void LoadRanks();
	//static void LoadsSprites();
	static void LoadMap(string mapName);
	static Colony * LoadColony(Team * team, int startpop, Celestial * celestial);
	static void AddNamedObject(NamedObject * n);
	//static void RemoveObject(NamedObject obj);

};