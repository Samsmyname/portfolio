#include "Celestial.h"

Celestial::Celestial()
{
	OrbitMeDistance = 1;
}

void Celestial::Update(double deltaTime)
{
	// cout << "Celestial Model Update." << endl;
	if (IsOrbiting())
		OrbitAdjustment(deltaTime);
}

void Celestial::SetDefaultOrbitDistance(int distance)
{
	OrbitMeDistance = distance;
}

Colony * Celestial::AttachedColony()
{
	return _Colony;
}

void Celestial::RemoveColony()
{
	//TODO safely delete
	//_Colony = nullptr;

	delete _Colony;
}

void Celestial::PlaceColony(Colony * colony)
{
	_Colony = colony;
}

float Celestial::DefaultOrbitDistance()
{
	return OrbitMeDistance;
}

vec2<float> Celestial::GetLocation()
{
	return ICanOrbitStatic::_Location;
}
