#pragma once

//#include "Celestial.h"
#include "vec2.h"

class Celestial;

class ICanOrbitStatic
{
private:
	Celestial * _OrbitingBody;
	float _OrbitAngle;
	float _OrbitDistance;
	

protected:
	vec2<float> _Location;

public:
	ICanOrbitStatic();
	void MakeOrbit(Celestial & orbitParent, float radius, float angle);
	void MakeOrbit(Celestial & orbitParent, float radius);
	void MakeOrbit(Celestial & orbitParent);
	void BreakOrbit();
	Celestial * GetOrbitingBody();
	float OrbitAdjustment(double deltaTime);
	float Angle();
	vec2<float> OrbitAngleToCoords();
	bool IsOrbiting();
	vec2<float> Location();
	//void SetLocation(vec2<float> location);
};