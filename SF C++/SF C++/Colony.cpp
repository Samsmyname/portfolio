#include "Colony.h"
#include "Celestial.h"

Colony::Colony(Team * team, int startpop)
{
	_Team = team;
	_Population = startpop;
	_StartPop = startpop;

	Rect.w = 30;
	Rect.h = 30;
}

void Colony::AttachTo(Celestial * c)
{
	_AttachedTo = c;
	c->PlaceColony(this);
	SetLocation(c->Location());
}

float Colony::Population()
{
	return _Population;
}

float Colony::Growth()
{
	return _Growth;
}

string Colony::Name()
{
	if (_AttachedTo != nullptr)
	{
		return _AttachedTo->Name();
	}
	return _Name;
}

void Colony::Destroy()
{
	if (_AttachedTo != nullptr)
	{
		_AttachedTo->RemoveColony();
		_AttachedTo = nullptr;
	}

	delete this;
	//TODO check
}

void Colony::Update(double deltaTime)
{
	if (_AttachedTo != nullptr)
	{
		SetLocation(_AttachedTo->Location());
	}

	if (_Population < _StartPop)
		_Population += (float)(_Population * _Growth * deltaTime);
}
