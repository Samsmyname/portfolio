#include <thread>

#include "SDL.h"

#include "UniverseController.h"
#include "View.h"
#include "Ship.h"
#include "Pilot.h"


int main(int argx, char *argv[])
{
	

	SDL_Init(SDL_INIT_EVERYTHING);

	const Uint8* keystates = SDL_GetKeyboardState(NULL);

	UniverseController::Load();
	View view;
	view.Setup();


	bool quit = false;

	SDL_Event event;

	while (!quit)
	{
		while (SDL_PollEvent(&event)) {
			switch (event.type)
			{
				case SDL_QUIT:
					quit = true;
					break;
				case SDL_KEYDOWN:
					switch (event.key.keysym.sym)
					{
					case SDLK_0:
						view.Zoom++;
						break;
					case SDLK_1:
						view.Zoom--;
						break;
					}

			}
		}

		UniverseController::UpKey = false;
		UniverseController::DownKey = false;
		UniverseController::LeftKey = false;
		UniverseController::RighKey = false;
		UniverseController::SpaceKey = false;

		if (keystates[SDL_SCANCODE_LEFT])
			UniverseController::LeftKey = true;
		if (keystates[SDL_SCANCODE_RIGHT])
			UniverseController::RighKey = true;
		if (keystates[SDL_SCANCODE_UP])
			UniverseController::UpKey = true;
		if (keystates[SDL_SCANCODE_DOWN])
			UniverseController::DownKey = true;
		if (keystates[SDL_SCANCODE_SPACE])
			UniverseController::SpaceKey = true;

		UniverseController::Tick();
		view.Update();

	}
	

	//ui.join();

	view.Shutdown();
	SDL_Quit();

	return 0;
}

void WaitForUserinput(View view, bool * quit)
{

}