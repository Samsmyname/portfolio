#include "PlanetFactory.h"
#include "Celestial.h"

Celestial * PlanetFactory::MakePlanet(string name, int defaultOrbit, Celestial * parent, int parentOrbitDistance, float angle)
{
	Celestial * planet = new Celestial();
	if (parent != nullptr)
	{
		planet->MakeOrbit(*parent, parentOrbitDistance, angle);
	}
	planet->NameObject(name);
	planet->SetDefaultOrbitDistance(defaultOrbit);

	return planet;
}

Celestial * PlanetFactory::MakePlanet(string name, int defaultOrbit, Celestial * parent, int parentOrbitDistance)
{
	Celestial * planet = MakePlanet(name, defaultOrbit, parent, parentOrbitDistance, 0);

	return planet;
}

Celestial * PlanetFactory::MakePlanet(string name, int defaultOrbit)
{
	Celestial * planet = MakePlanet(name, defaultOrbit, nullptr, 0, 0);

	return planet;
}
