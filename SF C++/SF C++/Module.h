#pragma once

#include "NamedObject.h"

enum class ModuleType
{
	Shield,
	Armour,
	Engine,
	Weapon
};

class Ship;

class Module : NamedObject
{
public:

private:
	int _Volume;
	int _Mass;
	int _EnergyUse;
	ModuleType _ModuleType;

public:
	Module(int volume, int mass);
	int Mass();
	virtual void OnUse(Ship * ship) = 0;
	virtual void Update(double deltaTime) = 0;
	ModuleType GetModuleType();
	void SetModuleType(ModuleType m);
};