#pragma once

#include "NamedObject.h"

#include <unordered_map>
#include <vector>

using namespace std;

class Colony;
class Pilot;

class Team : public NamedObject
{
private:
	int _ID;
	
	float _PilotRespawnTime;
	vector<Pilot *> _Pilots;
	vector<Colony *> _Colonies;
	Colony * _DefaultHome;
	float _PilotRespawnRemaining;

public:
	Team(string name, int ID);
	void SpawnPilotShip();
	int ID();
	unordered_map<string, string> Sprites;
	void AddPilot(Pilot * P);
	vector<Pilot *> Pilots();
	//vetor<PilotAI> AIPilots();
	void AddColony(Colony * c);
	vector<Colony *> Colonies();
	void SetHome(Colony * home);
	Colony * HomePlanet();
	void Strategise();
	void Update(double deltaTime);
};