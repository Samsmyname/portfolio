#include "View.h"
#include "SDL_image.h"
#include "UniverseController.h"
#include "PhysicalObject.h"
#include "Pilot.h"
#include "Ship.h"
#include <cmath>

void View::Update()
{

	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
	SDL_RenderClear(renderer);

	cameraX = (int)UniverseController::player.MyShip()->GetLocation().x - (0.5*cameraWidth/Zoom);
	cameraY = (int)UniverseController::player.MyShip()->GetLocation().y - (0.5*cameraHeight/Zoom);

	for (PhysicalObject * p : UniverseController::PhysicalObjects())
	{
		p->Rect.x = p->GetLocation().x - cameraX;
		p->Rect.y = p->GetLocation().y - cameraY;
		SDL_Rect tempRect = p->Rect;
		tempRect.w *= Zoom;
		tempRect.h *= Zoom;
		tempRect.x *= Zoom;
		tempRect.y *= Zoom;

		

		tempRect.x -= (tempRect.w / 2);
		tempRect.y -= (tempRect.h / 2);

		SDL_Point center = {tempRect.w/2, tempRect.h/2};
		SDL_RendererFlip flip = SDL_FLIP_NONE;
		float angle = 0;
		if (p->CurrentHeading.length() != 0)
		{
			angle = atan2(p->CurrentHeading.y, p->CurrentHeading.x)*180/3.1415926535897;
			angle += 90;
		}
		
		SDL_RenderCopyEx(renderer, sprites[p->SpriteString], NULL, &tempRect, angle, &center, flip);

		//cout << "View Updated." << endl;
	}

	//Copying the texture on to the window using renderer and rectangle
	//SDL_RenderCopy(renderer, texture, NULL, &rect);

	SDL_RenderPresent(renderer);

	
}

void View::Setup()
{

	IMG_Init(IMG_INIT_PNG);

	window = SDL_CreateWindow("title", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 800, 600, SDL_WINDOW_SHOWN);
	renderer = SDL_CreateRenderer(window, -1, 0);

	for (string r : UniverseController::_Resources)
	{
		SDL_Surface * surface = IMG_Load(r.c_str());
		SDL_Texture * texture = SDL_CreateTextureFromSurface(renderer, surface);
		sprites[r] = texture;
		SDL_FreeSurface(surface);
	}

	/*for (PhysicalObject * p : UniverseController::PhysicalObjects())
	{
		if (sprites.find(p->SpriteString) == sprites.end())
		{
			SDL_Surface * surface = IMG_Load(p->SpriteString.c_str());
			SDL_Texture * texture = SDL_CreateTextureFromSurface(renderer, surface);
			sprites[p->SpriteString] = texture;
			SDL_FreeSurface(surface);
		}

	}*/

	cameraHeight = 600;
	cameraWidth = 800;
	cameraX = 0;
	cameraY = 0;
	Zoom = 2;

	
}

void View::Shutdown()
{
	//Deleting the texture
	//SDL_DestroyTexture(texture);

	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);

	//For quitting IMG systems
	IMG_Quit();
}

void View::MoveCamera(int x, int y)
{
	cameraX += x;
	cameraY += y;
}
