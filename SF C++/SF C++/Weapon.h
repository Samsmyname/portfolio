#pragma once

#include "Module.h"
#include "vec2.h"

class Ship;

class Weapon : public Module
{

private:
	int _ContactDamage;
	float _Speed;
	int _LifeSpan;
	int _ReloadTime;
	float _TimeTillReload;
	string _Class;

public:
	Weapon(int volume, int mass, float speed, int lifeSpan, int damage, int reloadTime, string c);
	void OnUse(Ship * ship) override;
	void Update(double deltaTime) override;
};