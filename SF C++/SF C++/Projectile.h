#pragma once

#include "DestroyableObject.h"
//#include "Ship.h"

#include "vec2.h"


using namespace std;

class Ship;

class Projectile : public DestroyableObject
{
private:
	vec2<float> _Vector;
	int _ContactDamage;
	Ship * _Launcher;
	float _TimeTillDeath;
	DestroyableObject * CheckForContact();

public:
	Projectile(Ship * launcher, vec2<float> speed, int timeTillDeath, int damage, string c);
	void Update(double detalTime) override;
};