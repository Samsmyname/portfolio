#include "ModuleFactory.h"
#include "Module.h"
#include "Shield.h"
#include "Armour.h"
#include "Engine.h"
#include "Weapon.h"
#include "vec2.h"

Module * ModuleFactory::MakeWeapon()
{
	Module * weapon = new Weapon(100, 100, 200, 1000, 10, 1000, "Weapon");
	weapon->SetModuleType(ModuleType::Weapon);
	return weapon;
}

Module * ModuleFactory::MakeShield()
{
	Module * shield = new Shield(100, 100, 200);
	shield->SetModuleType(ModuleType::Shield);
	return shield;
}

Module * ModuleFactory::MakeArmour()
{
	Module * armour = new Armour(100, 100, 200);
	armour->SetModuleType(ModuleType::Armour);
	return armour;
}

Module * ModuleFactory::MakeEngine()
{
	Module * engine = new Engine(100, 100, 5);
	engine->SetModuleType(ModuleType::Engine);
	return engine;
}
