#include "Weapon.h"
#include "Projectile.h"
#include "Ship.h"
#include "UniverseController.h"

Weapon::Weapon(int volume, int mass, float speed, int lifeSpan, int damage, int reloadTime, string c) : Module(volume, mass)
{
	_Speed = speed/1000;
	_LifeSpan = lifeSpan;
	_ContactDamage = damage;
	_ReloadTime = reloadTime;
	_TimeTillReload = 0;
	_Class = c;

}

void Weapon::OnUse(Ship * ship)
{
	if (_TimeTillReload <= 0)
	{
		vec2<float> s;
		s.x = ship->CurrentHeading.x * _Speed;
		s.y = ship->CurrentHeading.y * _Speed;

		s.x += ship->CurrentSpeed.x;
		s.y += ship->CurrentSpeed.y;

		Projectile * p = new Projectile(ship, s, _LifeSpan, _ContactDamage, _Class);
		p->SpriteString = "Resources/Images/massdriver.png";
		UniverseController::AddPhysicalObject(p);
		_TimeTillReload = _ReloadTime;
	}
}

void Weapon::Update(double deltaTime)
{
	//cout << deltaTime << endl;
	_TimeTillReload -= deltaTime;
	if (_TimeTillReload < 0)
		_TimeTillReload = 0;
}
