#include "Team.h"
#include "Pilot.h"
#include "Colony.h"

Team::Team(string name, int ID)
{
	_Name = name;
	_ID = ID;
}

void Team::SpawnPilotShip()
{
	for (Pilot * p : _Pilots)
	{
		if (p->MyShip() == nullptr)
		{
			p->SpawnShip();
			_PilotRespawnRemaining = _PilotRespawnTime;
			break;
		}
	}
}

int Team::ID()
{
	return _ID;
}

void Team::AddPilot(Pilot * P)
{
	_Pilots.push_back(P);
}

vector<Pilot *> Team::Pilots()
{
	return _Pilots;
}

void Team::AddColony(Colony * c)
{
	_Colonies.push_back(c);
}

vector<Colony*> Team::Colonies()
{
	return _Colonies;
}

void Team::SetHome(Colony * home)
{
	_DefaultHome = home;
}

Colony * Team::HomePlanet()
{
	if (_DefaultHome != nullptr)
	{
		if (_DefaultHome->MyTeam() == this)
			return _DefaultHome;
		else
			return _Colonies[0];
	}
	else
	{
		//Type[] types = new Type[1];
		//types[0] = typeof(Colony);
		//return (UniverseController.GetClosestOfType(types, (UniverseController.GetObjectByName("Earth") as PhysicalObject).Location, null) as Colony);
	}
}

void Team::Strategise()
{
}

void Team::Update(double deltaTime)
{
}
