#include "ShipFactory.h"
#include "Ship.h"
#include "ModuleFactory.h"
#include "Pilot.h"
#include "Team.h"
#include "vec2.h"
#include "Colony.h"
#include "UniverseController.h"

Ship * ShipFactory::MakeDreadnaught(string name, Pilot * pilot)
{
	Ship * ship = new Ship();
	ship->SetClass("Dreadnaught");
	ship->SetPilot(pilot);
	ship->SetMaxModules(8);
	vector<Module *> defaultModules;
	defaultModules.push_back(ModuleFactory::MakeWeapon());
	defaultModules.push_back(ModuleFactory::MakeArmour());
	defaultModules.push_back(ModuleFactory::MakeShield());
	defaultModules.push_back(ModuleFactory::MakeEngine());
	ship->SetModules(defaultModules);
	ship->NameObject(name);
	ship->SetStartingTeam(pilot->MyTeam());
	string spritename = "Resources/Images/";
	spritename += "SpaceShipRed";
	spritename += ".png";
	ship->SpriteString = spritename;
	ship->Rect.w = 10;
	ship->Rect.h = 10;
	ship->SetLocation(pilot->MyTeam()->HomePlanet()->GetLocation());
	ship->CurrentHeading.y = -1;

	UniverseController::AddPhysicalObject(ship);

	return ship;
}
