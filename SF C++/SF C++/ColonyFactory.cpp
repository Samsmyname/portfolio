#include "ColonyFactory.h"
#include "Team.h"
#include "Colony.h"
#include "ModuleFactory.h"
#include "Celestial.h"



Colony * ColonyFactory::MakeColony(Team * team, int startPop, Celestial * celestial)
{
	Colony * colony = new Colony(team, startPop);
	colony->SetClass("Colony");
	colony->SetMaxModules(3);
	vector<Module *> defaultModules;
	defaultModules.push_back(ModuleFactory::MakeWeapon());
	defaultModules.push_back(ModuleFactory::MakeArmour());
	defaultModules.push_back(ModuleFactory::MakeShield());
	colony->SetModules(defaultModules);

	if (celestial != nullptr)
	{
		colony->AttachTo(celestial);
	}

	return colony;
}

Colony * ColonyFactory::MakeColony(Team * team, int startPop)
{
	return MakeColony(team, startPop, nullptr);
}
