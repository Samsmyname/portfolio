#pragma once

#include "NamedObject.h"
//#include "Team.h"
#include "vec2.h"
#include "SDL.h"
class Team;

class PhysicalObject : public NamedObject
{
protected:
	Team * _Team;
	string _Class;

	//void CheckAngle();
	
private:

	float _Angle;
	vec2<float> _Location;
	
	// _Lock?

public:
	PhysicalObject();
	string Class();
	void SetClass(string c);
	//virtual float Angle();
	//vec2<float> AngleAsVector();
	//virtual void SetAngle(float angle);

	virtual Team * MyTeam();
	virtual void Update(double deltaTime) = 0;

	void SetLocation(vec2<float> location);

	vec2<float> CurrentHeading;
	vec2<float> CurrentSpeed;

	virtual vec2<float> GetLocation();

	SDL_Rect Rect;
	string SpriteString;
};