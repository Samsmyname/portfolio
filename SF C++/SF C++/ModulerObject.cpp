#include "ModulerObject.h"
#include "Module.h"
#include "Shield.h"
#include "Armour.h"

float ModulerObject::Mass()
{
	float mass = 0;
	for (Module * m : _Modules)
	{
		mass += m->Mass();
	}

	return mass;
}

int ModulerObject::DamageShields(int dmg)
{
	for(Module * m : _Modules)
	{

		if (m->GetModuleType() == ModuleType::Shield)
		{
			Shield * s = dynamic_cast<Shield *>(m);
			dmg = s->DamageShield(dmg);
		}
	}

	return dmg;
}

int ModulerObject::DamageArmour(int dmg)
{
	for (Module * m : _Modules)
	{

		if (m->GetModuleType() == ModuleType::Armour)
		{
			Armour * a = dynamic_cast<Armour *>(m);
			dmg = a->DamageArmour(dmg);
		}
	}

	return dmg;
}

vector<Module *> ModulerObject::GetModules()
{
	return _Modules;
}

void ModulerObject::SetMaxModules(int number)
{
	//TODO
	//_Modules = new Module[number];
}

void ModulerObject::SetModules(vector<Module *> modules)
{
	if (_Modules.size() != 0)
	{
		throw std::invalid_argument("Tried to assign modules when they have already been assigned!");
	}
	//_Modules.clear();

	_Modules = modules;
}

int ModulerObject::ArmourPoints()
{
	int armourFromModules = 0;

	for (Module * m : _Modules)
	{

		if (m->GetModuleType() == ModuleType::Armour)
		{
			Armour * a = dynamic_cast<Armour *>(m);
			armourFromModules += a->ArmourPoints();
		}
	}

	return armourFromModules;
}

int ModulerObject::ArmourPointsMax()
{
	int armourFromModules = 0;

	for (Module * m : _Modules)
	{

		if (m->GetModuleType() == ModuleType::Armour)
		{
			Armour * a = dynamic_cast<Armour *>(m);
			armourFromModules += a->ArmourPointsMax();
		}
	}

	return armourFromModules;
}

int ModulerObject::ShieldPoints()
{
	int shieldFromModules = 0;

	for (Module * m : _Modules)
	{

		if (m->GetModuleType() == ModuleType::Shield)
		{
			Shield * s = dynamic_cast<Shield *>(m);
			shieldFromModules += s->ShieldPoints();
		}
	}

	return shieldFromModules;
}

int ModulerObject::ShieldPointsMax()
{
	int shieldFromModules = 0;

	for (Module * m : _Modules)
	{

		if (m->GetModuleType() == ModuleType::Shield)
		{
			Shield * s = dynamic_cast<Shield *>(m);
			shieldFromModules += s->ShieldPointsMax();
		}
	}

	return shieldFromModules;
}
