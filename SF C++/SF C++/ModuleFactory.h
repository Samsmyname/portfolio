#pragma once

//#include "Module.h"

class Module;

static class ModuleFactory
{
public:
	static Module * MakeWeapon();
	static Module * MakeShield();
	static Module * MakeArmour();
	static Module * MakeEngine();

};