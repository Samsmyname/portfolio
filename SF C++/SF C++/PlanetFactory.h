#pragma once

//#include "Celestial.h"
#include <string>

class Celestial;

using namespace std;

static class PlanetFactory
{
public:
	static Celestial * MakePlanet(string name, int defaultOrbit, Celestial * parent, int parentOrbitDistance, float angle);
	static Celestial * MakePlanet(string name, int defaultOrbit, Celestial * parent, int parentOrbitDistance);
	static Celestial * MakePlanet(string name, int defaultOrbit);
};