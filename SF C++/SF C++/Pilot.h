#pragma once

#include "NamedObject.h"
//#include "Ship.h"
//#include "Team.h"

class Ship;
class Team;
class PhysicalObject;
class DestroyableObject;

enum Rank
{
	Cadet,
	Admiral
};

class Pilot : NamedObject
{
private:
	Team * _Team;
	Rank _Rank;
	PhysicalObject * _Target;
	DestroyableObject * _DestroyableTarget;
	Ship * _Ship;
	bool _Human;

public:
	Pilot();
	Pilot(string name, Team * team, bool human);
	void SpawnShip();
	Ship * MyShip();
	void SetShip(Ship * ship);
	void SetTarget(PhysicalObject * tar);
	Team * MyTeam();
	bool HasTarget();
	bool HasDestroyableTarget();
	PhysicalObject * Target();
	DestroyableObject * DestroyableTarget();
	virtual void Update();
};
