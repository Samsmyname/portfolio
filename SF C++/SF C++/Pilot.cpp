#include "Pilot.h"
#include "Team.h"
#include "ShipFactory.h"
#include "DestroyableObject.h"
#include "Ship.h"

Pilot::Pilot()
{
	_Name = "PILOT_UNDEFINED";
	_Human = true;
	_Team = nullptr;
}

Pilot::Pilot(string name, Team * team, bool human)
{
	_Name = name;
	_Human = human;
	_Team = team;
	_Rank = Rank::Cadet;

	_Team->AddPilot(this);
}

void Pilot::SpawnShip()
{
	if (_Ship == nullptr)
	{
		SetShip(ShipFactory::MakeDreadnaught(Name() + "'s Ship", this));
	}
}

Ship * Pilot::MyShip()
{
	return _Ship;
}

void Pilot::SetShip(Ship * ship)
{
	_Ship = ship;
}

void Pilot::SetTarget(PhysicalObject * tar)
{
	_Target = tar;
}

Team * Pilot::MyTeam()
{
	return _Team;
}

bool Pilot::HasTarget()
{
	if (_Target != nullptr)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool Pilot::HasDestroyableTarget()
{
	if (_DestroyableTarget != nullptr)
	{
		return true;
	}
	else
	{
		return false;
	}
}

PhysicalObject * Pilot::Target()
{
	return _Target;
}

DestroyableObject * Pilot::DestroyableTarget()
{
	return _DestroyableTarget;
}



void Pilot::Update()
{
}
