#include "Module.h"

Module::Module(int volume, int mass)
{
	_Volume = volume;
	_Mass = mass;
}

int Module::Mass()
{
	return _Mass;
}

ModuleType Module::GetModuleType()
{
	return _ModuleType;
}

void Module::SetModuleType(ModuleType m)
{
	_ModuleType = m;
}
