#include "Engine.h"

Engine::Engine(int volume, int mass, int power) : Module(volume, mass)
{
	_Power = power;
}

int Engine::Power()
{
	return _Power;
}

void Engine::OnUse(Ship * ship)
{
}

void Engine::Update(double deltaTime)
{
}
