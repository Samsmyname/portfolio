#pragma once

#include "ModulerObject.h"
//#include "Engine.h"
//#include "Team.h"
//#include "Pilot.h"

//class Team;
class Pilot;
class Team;

class Ship : public ModulerObject
{
private:
	Pilot * _Pilot;
	int _Acceleration;
	float _RotationalSpeed;
	int _MaxSpeed;


public:
	Ship();
	Team * MyTeam() override;
	void SetPilot(Pilot * P);
	void SetStartingTeam(Team * t);
	void AIMove();
	void RotateLeft();
	void RotateRight();
	float GetAcceleration();
	void Accelerate();
	void Reverse();
	void Brake();
	void FireWeapons();
	void Update(double deltaTime) override;
};