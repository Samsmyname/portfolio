package org.samuelaholmes.terraformingmars;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.HashMap;

/**
 * Created by samue on 3/07/2018.
 */

public class DemoCollectionPagerAdapter extends FragmentPagerAdapter {

    Fragment income;
    Fragment settingsPage;
    Fragment transactionTable;
    Fragment networkActivity;

    public DemoCollectionPagerAdapter(FragmentManager fm, Fragment income, Fragment settingsPage, Fragment transactionTable, Fragment networkActivity) {
        super(fm);
        this.income = income;
        this.settingsPage = settingsPage;
        this.transactionTable = transactionTable;
        this.networkActivity = networkActivity;
    }

    @Override
    public Fragment getItem(int i) {

        if (i == 0)
            return settingsPage;
        else if (i == 1)
            return income;
        else if (i == 2)
            return transactionTable;
        return income;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "OBJECT " + (position + 1);
    }
}
