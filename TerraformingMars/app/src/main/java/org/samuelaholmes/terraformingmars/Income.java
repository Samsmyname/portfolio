package org.samuelaholmes.terraformingmars;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class Income extends Fragment{

    HashMap<String, ResourceView> resourceViewMap = new HashMap<String, ResourceView>();

    private TextView terraformRatingv;
    private TextView moneyCurrentv;
    private TextView moneyProductionv;
    private TextView steelCurrentv;
    private TextView steelProductionv;
    private TextView titaniumCurrentv;
    private TextView titaniumProductionv;
    private TextView plantsCurrentv;
    private TextView plantsProductionv;
    private TextView energyCurrentv;
    private TextView energyProductionv;
    private TextView heatCurrentv;
    private TextView heatProductionv;
    private TextView generationv;
    private TextView totalValuev;
    private ImageView plantsv;
    private ImageView heatv;

    private boolean plantsSelected;
    private boolean heatSelected;

    DataStructure myData;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {

        View v = inflater.inflate(R.layout.activity_income, container, false);

        terraformRatingv = (TextView) v.findViewById(R.id.terraformRating);
        moneyCurrentv = (TextView) v.findViewById(R.id.moneyCurrent);
        moneyProductionv = (TextView) v.findViewById(R.id.moneyProduction);
        steelCurrentv = (TextView) v.findViewById(R.id.steelCurrent);
        steelProductionv = (TextView) v.findViewById(R.id.steelProduction);
        titaniumCurrentv = (TextView) v.findViewById(R.id.titaniumCurrent);
        titaniumProductionv = (TextView) v.findViewById(R.id.titaniumProduction);
        plantsCurrentv = (TextView) v.findViewById(R.id.plantsCurrent);
        plantsProductionv = (TextView) v.findViewById(R.id.plantsProduction);
        energyCurrentv = (TextView) v.findViewById(R.id.energyCurrent);
        energyProductionv = (TextView) v.findViewById(R.id.energyProduction);
        heatCurrentv = (TextView) v.findViewById(R.id.heatCurrent);
        heatProductionv = (TextView) v.findViewById(R.id.heatProduction);
        generationv = (TextView) v.findViewById(R.id.generation);
        totalValuev = (TextView) v.findViewById(R.id.feedback);
        plantsv = (ImageView) v.findViewById(R.id.plants);
        heatv = (ImageView) v.findViewById(R.id.heat);

        resourceViewMap.get("TerraformRating").AttachTextView(terraformRatingv);
        resourceViewMap.get("CurrentMoney").AttachTextView(moneyCurrentv);
        resourceViewMap.get("ProductionMoney").AttachTextView(moneyProductionv);
        resourceViewMap.get("CurrentSteel").AttachTextView(steelCurrentv);
        resourceViewMap.get("ProductionSteel").AttachTextView(steelProductionv);
        resourceViewMap.get("CurrentTitanium").AttachTextView(titaniumCurrentv);
        resourceViewMap.get("ProductionTitanium").AttachTextView(titaniumProductionv);
        resourceViewMap.get("CurrentPlants").AttachTextView(plantsCurrentv);
        resourceViewMap.get("ProductionPlants").AttachTextView(plantsProductionv);
        resourceViewMap.get("CurrentEnergy").AttachTextView(energyCurrentv);
        resourceViewMap.get("ProductionEnergy").AttachTextView(energyProductionv);
        resourceViewMap.get("CurrentHeat").AttachTextView(heatCurrentv);
        resourceViewMap.get("ProductionHeat").AttachTextView(heatProductionv);
        resourceViewMap.get("Generation").AttachTextView(generationv);

        updateText();
        for (ResourceView entry : resourceViewMap.values())
        {
            entry.updateBackground();
        }

        return v;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void reset()
    {
        resourceViewMap.clear();

        resourceViewMap.put("TerraformRating", new ResourceView("TerraformRating", terraformRatingv, myData));
        resourceViewMap.put("CurrentMoney", new ResourceView("CurrentMoney", moneyCurrentv, myData));
        resourceViewMap.put("ProductionMoney", new ResourceView("ProductionMoney", moneyProductionv, myData));
        resourceViewMap.put("CurrentSteel", new ResourceView("CurrentSteel", steelCurrentv, myData));
        resourceViewMap.put("ProductionSteel", new ResourceView("ProductionSteel", steelProductionv, myData));
        resourceViewMap.put("CurrentTitanium", new ResourceView("CurrentTitanium", titaniumCurrentv, myData));
        resourceViewMap.put("ProductionTitanium", new ResourceView("ProductionTitanium", titaniumProductionv, myData));
        resourceViewMap.put("CurrentPlants", new ResourceView("CurrentPlants", plantsCurrentv, myData));
        resourceViewMap.put("ProductionPlants", new ResourceView("ProductionPlants", plantsProductionv, myData));
        resourceViewMap.put("CurrentEnergy", new ResourceView("CurrentEnergy", energyCurrentv, myData));
        resourceViewMap.put("ProductionEnergy", new ResourceView("ProductionEnergy", energyProductionv, myData));
        resourceViewMap.put("CurrentHeat", new ResourceView("CurrentHeat", heatCurrentv, myData));
        resourceViewMap.put("ProductionHeat", new ResourceView("ProductionHeat", heatProductionv, myData));
        resourceViewMap.put("Generation", new ResourceView("Generation", generationv, myData));

    }

    public void attachData(DataStructure dataStructure)
    {
        this.myData = dataStructure;

        resourceViewMap.put("TerraformRating", new ResourceView("TerraformRating", terraformRatingv, dataStructure));
        resourceViewMap.put("CurrentMoney", new ResourceView("CurrentMoney", moneyCurrentv, dataStructure));
        resourceViewMap.put("ProductionMoney", new ResourceView("ProductionMoney", moneyProductionv, dataStructure));
        resourceViewMap.put("CurrentSteel", new ResourceView("CurrentSteel", steelCurrentv, dataStructure));
        resourceViewMap.put("ProductionSteel", new ResourceView("ProductionSteel", steelProductionv, dataStructure));
        resourceViewMap.put("CurrentTitanium", new ResourceView("CurrentTitanium", titaniumCurrentv, dataStructure));
        resourceViewMap.put("ProductionTitanium", new ResourceView("ProductionTitanium", titaniumProductionv, dataStructure));
        resourceViewMap.put("CurrentPlants", new ResourceView("CurrentPlants", plantsCurrentv, dataStructure));
        resourceViewMap.put("ProductionPlants", new ResourceView("ProductionPlants", plantsProductionv, dataStructure));
        resourceViewMap.put("CurrentEnergy", new ResourceView("CurrentEnergy", energyCurrentv, dataStructure));
        resourceViewMap.put("ProductionEnergy", new ResourceView("ProductionEnergy", energyProductionv, dataStructure));
        resourceViewMap.put("CurrentHeat", new ResourceView("CurrentHeat", heatCurrentv, dataStructure));
        resourceViewMap.put("ProductionHeat", new ResourceView("ProductionHeat", heatProductionv, dataStructure));
        resourceViewMap.put("Generation", new ResourceView("Generation", generationv, dataStructure));


    }

    public void modify(View view)
    {

        int x = Integer.parseInt(view.getTag().toString());

        for (ResourceView r : resourceViewMap.values()) {
            if (r.isSelected())
            {
                r.modify(x);
                updateText();
            }
        }

        int total = 0;

        for (ResourceView r : resourceViewMap.values()) {
            if (r.isModified())
            {
                total += (r.data.getModifiedDifferenceValue());
            }
        }

        totalValuev.setText("Total Value: " + total);

        updateText();
    }

    public void confirmModify(View view)
    {
        Transaction t = new Transaction(
                (resourceViewMap.get("TerraformRating")).data.getModifiedDifference(),
                (resourceViewMap.get("CurrentMoney")).data.getModifiedDifference(),
                (resourceViewMap.get("ProductionMoney")).data.getModifiedDifference(),
                (resourceViewMap.get("CurrentSteel")).data.getModifiedDifference(),
                (resourceViewMap.get("ProductionSteel")).data.getModifiedDifference(),
                (resourceViewMap.get("CurrentTitanium")).data.getModifiedDifference(),
                (resourceViewMap.get("ProductionTitanium")).data.getModifiedDifference(),
                (resourceViewMap.get("CurrentPlants")).data.getModifiedDifference(),
                (resourceViewMap.get("ProductionPlants")).data.getModifiedDifference(),
                (resourceViewMap.get("CurrentEnergy")).data.getModifiedDifference(),
                (resourceViewMap.get("ProductionEnergy")).data.getModifiedDifference(),
                (resourceViewMap.get("CurrentHeat")).data.getModifiedDifference(),
                (resourceViewMap.get("ProductionHeat")).data.getModifiedDifference(),
                0
        );

        myData.transactions.add(t);

        for (ResourceView r : resourceViewMap.values())
        {
            r.confirmModify();
            totalValuev.setText("Confirmed.");
            updateText();
        }
    }

    public void revertModify(View view)
    {
        for (ResourceView r : resourceViewMap.values())
        {
            r.revertModify();
            updateText();
        }
    }

    public void updateText()
    {
        if (terraformRatingv != null) {

            terraformRatingv.setText("" + ( resourceViewMap.get("TerraformRating")).data.getModifiedAmount());
            moneyCurrentv.setText("" + ( resourceViewMap.get("CurrentMoney")).data.getModifiedAmount());
            moneyProductionv.setText("+" + ( resourceViewMap.get("ProductionMoney")).data.getModifiedAmount());
            steelCurrentv.setText("" + ( resourceViewMap.get("CurrentSteel")).data.getModifiedAmount());
            steelProductionv.setText("+" + ( resourceViewMap.get("ProductionSteel")).data.getModifiedAmount());
            titaniumCurrentv.setText("" + ( resourceViewMap.get("CurrentTitanium")).data.getModifiedAmount());
            titaniumProductionv.setText("+" + ( resourceViewMap.get("ProductionTitanium")).data.getModifiedAmount());
            plantsCurrentv.setText("" + ( resourceViewMap.get("CurrentPlants")).data.getModifiedAmount());
            plantsProductionv.setText("+" + ( resourceViewMap.get("ProductionPlants")).data.getModifiedAmount());
            energyCurrentv.setText("" + ( resourceViewMap.get("CurrentEnergy")).data.getModifiedAmount());
            energyProductionv.setText("+" + ( resourceViewMap.get("ProductionEnergy")).data.getModifiedAmount());
            heatCurrentv.setText("" + ( resourceViewMap.get("CurrentHeat")).data.getModifiedAmount());
            heatProductionv.setText("+" + ( resourceViewMap.get("ProductionHeat")).data.getModifiedAmount());
            generationv.setText("" + ( resourceViewMap.get("Generation")).data.getModifiedAmount());
        }
    }

    public void selectResource(View view) {

        for (ResourceView r : resourceViewMap.values())
        {
            r.deselect();
        }

        GradientDrawable border = new GradientDrawable();
        border.setStroke(8, 0xFF000000); //black border with full opacity

        GradientDrawable noBorder = new GradientDrawable();
        int c = Color.argb(255, 63, 81, 181);
        border.setColor(c);


        plantsv.setBackground(noBorder);
        heatv.setBackground(noBorder);

        if (view == plantsv)
        {
            plantsv.setBackground(border);
            if (!plantsSelected)
                plantsSelected = true;
            else
            {
                (resourceViewMap.get("CurrentPlants")).modify(myData.getPlantValue()*-1);
                updateText();
            }
        }
        else
        {
            plantsSelected = false;
        }

        if (view == heatv)
        {
            heatv.setBackground(border);
            if (!heatSelected)
                heatSelected = true;
            else
            {
                (resourceViewMap.get("CurrentHeat")).modify(myData.heatForATemperature*-1);
                updateText();
            }
        }
        else
        {
            heatSelected = false;
        }

        if (view == terraformRatingv)
        {
            (resourceViewMap.get("TerraformRating")).select();
        }
        else if (view == moneyCurrentv)
        {
            (resourceViewMap.get("CurrentMoney")).select();
        }
        else if (view == steelCurrentv)
        {
            (resourceViewMap.get("CurrentSteel")).select();
        }
        else if (view == titaniumCurrentv)
        {
            (resourceViewMap.get("CurrentTitanium")).select();
        }
        else if (view == plantsCurrentv)
        {
            (resourceViewMap.get("CurrentPlants")).select();
        }
        else if (view == energyCurrentv)
        {
            (resourceViewMap.get("CurrentEnergy")).select();
        }
        else if (view == heatCurrentv)
        {
            (resourceViewMap.get("CurrentHeat")).select();
        }
        else if (view == moneyProductionv)
        {
            (resourceViewMap.get("ProductionMoney")).select();
        }
        else if (view == steelProductionv)
        {
            (resourceViewMap.get("ProductionSteel")).select();
        }
        else if (view == titaniumProductionv)
        {
            (resourceViewMap.get("ProductionTitanium")).select();
        }
        else if (view == plantsProductionv)
        {
            (resourceViewMap.get("ProductionPlants")).select();
        }
        else if (view == energyProductionv)
        {
            (resourceViewMap.get("ProductionEnergy")).select();
        }
        else if (view == heatProductionv)
        {
            (resourceViewMap.get("ProductionHeat")).select();
        }

    }

}

