package org.samuelaholmes.terraformingmars;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import org.samuelaholmes.terraformingmars.Network.Server;

import java.util.HashMap;

/**
 * Created by samue on 3/07/2018.
 */

public class MainActivity extends AppCompatActivity{

    private static final String TAG = "TerraformingMars";
    public String name = "NOT_SET";
    private boolean amServer = false;
    public boolean shuttingDown = false;

    DemoCollectionPagerAdapter pagerAdapter;
    ViewPager viewPager;

    Income income;
    Multiplayer multiplayer;
    TransactionTable transactionTable;
    NetworkActivity networkActivity;

    Server server;
    public TextView msg;

    public DataStructure myData = new DataStructure();
    public HashMap<String, DataStructure> multiPlayerData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        myData.reset();
        myData.onCreate(this, savedInstanceState);
        myData.addActivity(this);
        multiPlayerData = new HashMap<>();

        income = new Income();
        income.attachData(myData);
        multiplayer = new Multiplayer();
        multiplayer.attachData(myData, this);
        transactionTable = new TransactionTable();
        transactionTable.attachData(myData);
        networkActivity = new NetworkActivity();
        networkActivity.setActivity(this);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        setContentView(R.layout.main_screen);
        pagerAdapter = new DemoCollectionPagerAdapter(getSupportFragmentManager(), income, multiplayer, transactionTable, networkActivity);
        viewPager = (ViewPager) findViewById(R.id.pager);
        viewPager.setAdapter(pagerAdapter);
        viewPager.setCurrentItem(1);

    }

    public void updatePlayers()
    {
        multiplayer.displayButtons();
    }

    public void showConnected()
    {
        multiplayer.buttonConnect.setBackgroundColor(Color.GREEN);
    }
    public void showDisconnected()
    {
        if (!amServer)
            multiplayer.showDisconnected();
    }


    public boolean host()
    {
        if (server == null) {
            amServer = true;
            server = new Server(this);
            multiplayer.buttonHost.setBackgroundColor(Color.GREEN);
            return true;
        }
        return false;
    }

    public void settings(View view)
    {
        Intent intent = new Intent(this, SettingsActivity.class);
        intent.putExtra("SteelValue", String.valueOf(myData.steelValue));
        intent.putExtra("TitaniumValue", String.valueOf(myData.titaniumValue));
        intent.putExtra("PlantValue", String.valueOf(myData.getPlantValue()));
        startActivityForResult(intent, 0);
    }

    @Override
    protected void onStart() {
        Log.d(TAG, "Starting.");
        super.onStart();
    }

    @Override
    public void onPause() {
        myData.onPause(this);
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (server!=null)
            server.onDestroy();
        if (multiplayer!=null)
            multiplayer.onStopObject();
    }
    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState)
    {
        myData.onSaveInstanceState(savedInstanceState);
    }
    @Override
    protected void onDestroy() {
        Log.d(TAG, "Being destroyed.");
        shuttingDown = true;
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0) {
            if (resultCode == 0) {
                myData.setSteelValue(Integer.parseInt(data.getStringExtra("SteelValue")));
                myData.setTitaniumValue(Integer.parseInt(data.getStringExtra("TitaniumValue")));
                myData.setPlantValue(Integer.parseInt(data.getStringExtra("PlantValue")));
            }
        }
    }

    public void modify(View view)
    {
        income.modify(view);
    }

    public void selectResource(View view)
    {
        income.selectResource(view);
    }

    public void newGeneration(View view)
    {
        final TransactionTable tTable = transactionTable;
        new AlertDialog.Builder(this)
                .setTitle("Next Generation")
                .setMessage("Are you sure?")
                .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        (myData.resourceMap.get("CurrentHeat")).modify((myData.resourceMap.get("CurrentEnergy")).getModifiedAmount());
                        (myData.resourceMap.get("CurrentEnergy")).overrideValue(0);

                        (myData.resourceMap.get("CurrentMoney")).modify((myData.resourceMap.get("ProductionMoney")).getModifiedAmount() + (myData.resourceMap.get("TerraformRating")).getModifiedAmount());
                        (myData.resourceMap.get("CurrentSteel")).modify((myData.resourceMap.get("ProductionSteel")).getModifiedAmount());
                        (myData.resourceMap.get("CurrentTitanium")).modify((myData.resourceMap.get("ProductionTitanium")).getModifiedAmount());
                        (myData.resourceMap.get("CurrentPlants")).modify((myData.resourceMap.get("ProductionPlants")).getModifiedAmount());
                        (myData.resourceMap.get("CurrentEnergy")).modify((myData.resourceMap.get("ProductionEnergy")).getModifiedAmount());
                        (myData.resourceMap.get("CurrentHeat")).modify((myData.resourceMap.get("ProductionHeat")).getModifiedAmount());

                        (myData.resourceMap.get("Generation")).modify(1);

                        for (TResourceDataOnly r : myData.resourceMap.values())
                        {
                            r.confirmModify();
                        }

                        Transaction t = new Transaction(
                                0,0,0,0,0,0,0,
                                0,0,0,0,0,0, 1
                        );
                        myData.transactions.add(t);
                        tTable.FillTable();
                        income.updateText();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();


        WindowManager.LayoutParams attrs = getWindow().getAttributes();
        attrs.flags |= WindowManager.LayoutParams.FLAG_FULLSCREEN;
        getWindow().setAttributes(attrs);

        myData.onPause(this);

    }

    public void reset(View view)
    {

        new AlertDialog.Builder(this)
                .setTitle("Reset")
                .setMessage("Are you sure?")
                .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        SharedPreferences data = getSharedPreferences("TERRAFORMINGMARS", 0);
                        data.edit().clear().commit();


                        myData.reset();
                        income.reset();
                        multiplayer.reset();
                        income.updateText();

                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();

    }

    public void revertModify(View view)
    {
        income.revertModify(view);
    }

    public void confirmModify(View view)
    {
        income.confirmModify(view);
        transactionTable.FillTable();
    }

}
