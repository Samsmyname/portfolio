package org.samuelaholmes.terraformingmars;

/**
 * Created by samue on 3/07/2018.
 */

public class Transaction {
    int g = 0;
    int trChange = 0;
    int moneyChange = 0;
    int moneyProductionchange = 0;
    int steelChange = 0;
    int steelProductionChange = 0;
    int titaniumChange = 0;
    int titaniumProductionChange = 0;
    int plantsChange = 0;
    int plantsProductionChange = 0;
    int powerChange = 0;
    int powerProductionChange = 0;
    int heatChange = 0;
    int heatProductionChange = 0;

    public Transaction(int trChange, int moneyChange, int moneyProductionchange, int steelChange, int steelProductionChange, int titaniumChange, int titaniumProductionChange,
                       int plantsChange, int plantsProductionChange, int powerChange, int powerProductionChange, int heatChange, int heatProductionChange, int g)
    {
        this.g = g;
        this.trChange = trChange;
        this.moneyChange = moneyChange;
        this.moneyProductionchange = moneyProductionchange;
        this.steelChange = steelChange;
        this.steelProductionChange = steelProductionChange;
        this.titaniumChange = titaniumChange;
        this.titaniumProductionChange = titaniumProductionChange;
        this.plantsChange = plantsChange;
        this.plantsProductionChange = plantsProductionChange;
        this.powerChange = powerChange;
        this.powerProductionChange = powerProductionChange;
        this.heatChange = heatChange;
        this.heatProductionChange = heatProductionChange;

    }

}
