package org.samuelaholmes.terraformingmars;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.widget.TextView;

import org.w3c.dom.Text;

/**
 * Created by samue on 9/07/2018.
 */

public class ResourceView {

    String name;
    TResourceDataOnly data;
    boolean selected;
    TextView tv;


    public ResourceView(String name, TextView tv, DataStructure data)
    {
        this.name = name;
        this.data = data.resourceMap.get(name);
        this.selected = false;
    }

    public void AttachTextView(TextView tv)
    {
        this.tv = tv;
    }

    public void modify(int x)
    {
        data.modify(x);
        updateBackground();
    }

    public void confirmModify()
    {
        data.confirmModify();
        updateBackground();
    }
    public void revertModify()
    {
        data.revertModify();
        updateBackground();
    }

    public void select()
    {
        selected = true;
        updateBackground();
    }

    public void deselect()
    {
        selected = false;
        updateBackground();
    }

    public void updateBackground()
    {

        if (tv !=null) {

            if (selected) {
                int c;
                if (isModified()) {

                    //c = Color.argb(100, 255, 165, 0);
                    c = Color.argb(100, 255, 255, 0);
                } else {
                    //c = Color.argb(100, 100, 200, 0);
                    c = Color.argb(50, 255, 255, 255);
                }

                //use a GradientDrawable with only one color set, to make it a solid color
                GradientDrawable border = new GradientDrawable();
                border.setColor(c);
                border.setStroke(5, 0xFF000000); //black border with full opacity
                tv.setBackground(border);


            /*if (isModified()) {
                tv.setBackgroundColor(Color.argb(100, 255, 165, 0));
            } else {
                tv.setBackgroundColor(Color.argb(100, 100, 200, 0));
            }*/
            } else {
                if (isModified()) {
                    tv.setBackgroundColor(Color.argb(100, 255, 255, 0));
                } else {
                    tv.setBackgroundColor(Color.argb(50, 255, 255, 255));
                }
            }
        }
    }

    public boolean isModified()
    {
        return data.isModified();
    }
    public boolean isSelected()
    {
        return selected;
    }
}
