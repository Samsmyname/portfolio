package org.samuelaholmes.terraformingmars.Network;

import org.samuelaholmes.terraformingmars.MainActivity;

import java.net.Socket;
import java.util.concurrent.TimeUnit;

public class ReceiveThread extends Thread {
    Socket socket;
    MainActivity activity;

    ReceiveThread(Socket socket, MainActivity activity) {
        this.socket = socket;
        this.activity = activity;
    }

    @Override
    public void run() {
        try {
            if (socket != null && !socket.isClosed()) {
                NetworkUtilities.ReceiveData(socket, activity.multiPlayerData);
                activity.updatePlayers();
                TimeUnit.SECONDS.sleep(1);
                if (!activity.shuttingDown)
                    this.run();
            }
            else
            {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        activity.showDisconnected();
                    }
                });
            }


        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
