package org.samuelaholmes.terraformingmars;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by samue on 9/07/2018.
 */

public class DataStructure {
    private MainActivity activity;
    public ArrayList<Transaction> transactions;
    public HashMap<String, TResourceDataOnly> resourceMap;

    private int plantsForAGreenery;
    public int heatForATemperature;
    int steelValue = 2;
    int titaniumValue = 3;

    public String constructMessage(String name)
    {
        String message = "-D-";

        message+= name;
        message += "*";
        message+= "RMap:";

        try{
            message += String.valueOf(resourceMap.get("Generation").getAmount()); //"RMap:2"
            message += "_";   //"RMap:2_"
            message += String.valueOf(resourceMap.get("TerraformRating").getAmount()); //"RMap:2"
            message += "_";   //"RMap:2_"
            message += String.valueOf(resourceMap.get("CurrentMoney").getAmount()); //"RMap:2"
            message += "_";   //"RMap:2_"
            message += String.valueOf(resourceMap.get("ProductionMoney").getAmount()); //"RMap:2"
            message += "_";   //"RMap:2_"
            message += String.valueOf(resourceMap.get("CurrentSteel").getAmount()); //"RMap:2"
            message += "_";   //"RMap:2_"
            message += String.valueOf(resourceMap.get("ProductionSteel").getAmount()); //"RMap:2"
            message += "_";   //"RMap:2_"
            message += String.valueOf(resourceMap.get("CurrentTitanium").getAmount()); //"RMap:2"
            message += "_";   //"RMap:2_"
            message += String.valueOf(resourceMap.get("ProductionTitanium").getAmount()); //"RMap:2"
            message += "_";   //"RMap:2_"
            message += String.valueOf(resourceMap.get("CurrentPlants").getAmount()); //"RMap:2"
            message += "_";   //"RMap:2_"
            message += String.valueOf(resourceMap.get("ProductionPlants").getAmount()); //"RMap:2"
            message += "_";   //"RMap:2_"
            message += String.valueOf(resourceMap.get("CurrentEnergy").getAmount()); //"RMap:2"
            message += "_";   //"RMap:2_"
            message += String.valueOf(resourceMap.get("ProductionEnergy").getAmount()); //"RMap:2"
            message += "_";   //"RMap:2_"
            message += String.valueOf(resourceMap.get("CurrentHeat").getAmount()); //"RMap:2"
            message += "_";   //"RMap:2_"
            message += String.valueOf(resourceMap.get("ProductionHeat").getAmount()); //"RMap:2"
            message += "_";   //"RMap:2_"
            Log.d("constructMessage", message);
        }
        catch (NullPointerException e)
        {
            message = name + "*RMap:0_0_0_0_0_0_0_0_0_0_0_0_0_0_";
        }


        return message;
    }

    public void deconstructMessage(String message)
    {
        reset();

        if (message.length() >= 5)
            if (message.substring(0, 5).equals("RMap:"))
            {
                message = message.substring(5, message.length());
                String[] resources = message.split("_");
                if (resources.length == 14) {
                    ArrayList<Integer> values = new ArrayList<>();
                    for (String r : resources) {
                        values.add(Integer.parseInt(r));
                    }

                    resourceMap.get("Generation").overrideValue(values.get(0));
                    resourceMap.get("TerraformRating").overrideValue(values.get(1));
                    resourceMap.get("CurrentMoney").overrideValue(values.get(2));
                    resourceMap.get("ProductionMoney").overrideValue(values.get(3));
                    resourceMap.get("CurrentSteel").overrideValue(values.get(4));
                    resourceMap.get("ProductionSteel").overrideValue(values.get(5));
                    resourceMap.get("CurrentTitanium").overrideValue(values.get(6));
                    resourceMap.get("ProductionTitanium").overrideValue(values.get(7));
                    resourceMap.get("CurrentPlants").overrideValue(values.get(8));
                    resourceMap.get("ProductionPlants").overrideValue(values.get(9));
                    resourceMap.get("CurrentEnergy").overrideValue(values.get(10));
                    resourceMap.get("ProductionEnergy").overrideValue(values.get(11));
                    resourceMap.get("CurrentHeat").overrideValue(values.get(12));
                    resourceMap.get("ProductionHeat").overrideValue(values.get(13));
                }

            }
    }

    public DataStructure()
    {
        reset();
    }

    public void addActivity(MainActivity activity)
    {
        this.activity = activity;
    }

    public void setPlantValue(int v)
    {
        plantsForAGreenery = v;
    }
    public int getPlantValue()
    {
        return plantsForAGreenery;
    }
    public void setSteelValue(int v)
    {
        steelValue = v;
        resourceMap.get("CurrentSteel").setValue(v);
    }
    public void setTitaniumValue(int v)
    {
        titaniumValue = v;
        resourceMap.get("CurrentTitanium").setValue(v);
    }

    public void reset()
    {
        plantsForAGreenery = 8;
        heatForATemperature = 8;
        steelValue = 2;
        titaniumValue = 3;

        resourceMap = new HashMap<String, TResourceDataOnly>();
        transactions = new ArrayList<>();

        resourceMap.put("Generation", new TResourceDataOnly(false, 0));
        resourceMap.put("TerraformRating", new TResourceDataOnly(true, 0));
        resourceMap.put("CurrentMoney", new TResourceDataOnly(true, 1));
        resourceMap.put("ProductionMoney", new TResourceDataOnly(true, 0));
        resourceMap.put("CurrentSteel", new TResourceDataOnly(true, 2));
        resourceMap.put("ProductionSteel", new TResourceDataOnly(true, 0));
        resourceMap.put("CurrentTitanium", new TResourceDataOnly(true, 3));
        resourceMap.put("ProductionTitanium", new TResourceDataOnly(true, 0));
        resourceMap.put("CurrentPlants", new TResourceDataOnly(true, 0));
        resourceMap.put("ProductionPlants", new TResourceDataOnly (true, 0));
        resourceMap.put("CurrentEnergy", new TResourceDataOnly(true, 0));
        resourceMap.put("ProductionEnergy", new TResourceDataOnly(true, 0));
        resourceMap.put("CurrentHeat", new TResourceDataOnly(true, 0));
        resourceMap.put("ProductionHeat", new TResourceDataOnly(true, 0));


        resourceMap.get("Generation").overrideValue(1);
        resourceMap.get("TerraformRating").overrideValue(20);

    }

    public void revertTranscation(int i)
    {

        Transaction t = transactions.get(i);

        if (t.trChange != 0)
            (resourceMap.get("TerraformRating")).modify(t.trChange);
        (resourceMap.get("TerraformRating")).confirmModify();
        if (t.moneyChange != 0)
            (resourceMap.get("CurrentMoney")).modify(t.moneyChange);
        (resourceMap.get("CurrentMoney")).confirmModify();
        if (t.moneyProductionchange != 0)
            (resourceMap.get("ProductionMoney")).modify(t.moneyProductionchange);
        (resourceMap.get("ProductionMoney")).confirmModify();
        if (t.steelChange != 0)
            (resourceMap.get("CurrentSteel")).modify(t.steelChange);
        (resourceMap.get("CurrentSteel")).confirmModify();
        if (t.steelProductionChange != 0)
            (resourceMap.get("ProductionSteel")).modify(t.steelProductionChange);
        (resourceMap.get("ProductionSteel")).confirmModify();
        if (t.titaniumChange != 0)
            (resourceMap.get("CurrentTitanium")).modify(t.titaniumChange);
        (resourceMap.get("CurrentTitanium")).confirmModify();
        if (t.titaniumProductionChange != 0)
            (resourceMap.get("ProductionTitanium")).modify(t.titaniumProductionChange);
        (resourceMap.get("ProductionTitanium")).confirmModify();
        if (t.plantsChange != 0)
            (resourceMap.get("CurrentPlants")).modify(t.plantsChange);
        (resourceMap.get("CurrentPlants")).confirmModify();
        if (t.plantsProductionChange != 0)
            (resourceMap.get("ProductionPlants")).modify(t.plantsProductionChange);
        (resourceMap.get("ProductionPlants")).confirmModify();
        if (t.powerChange != 0)
            (resourceMap.get("CurrentEnergy")).modify(t.powerChange);
        (resourceMap.get("CurrentEnergy")).confirmModify();;
        if (t.powerProductionChange != 0)
            (resourceMap.get("ProductionEnergy")).modify(t.powerProductionChange);
        (resourceMap.get("ProductionEnergy")).confirmModify();
        if (t.heatChange != 0)
            (resourceMap.get("CurrentHeat")).modify(t.heatChange);
        (resourceMap.get("CurrentHeat")).confirmModify();
        if (t.heatProductionChange != 0)
            (resourceMap.get("ProductionHeat")).modify(t.heatProductionChange);
        (resourceMap.get("ProductionHeat")).confirmModify();

        transactions.remove(i);
        if (activity != null)
            activity.income.updateText();
    }

    public void onCreate(Activity activity, Bundle savedInstanceState)
    {
        if (savedInstanceState == null) {
            // Restore data

            SharedPreferences data = activity.getSharedPreferences("TERRAFORMINGMARS", 0);

            (resourceMap.get("TerraformRating")).overrideValue(data.getInt("TerraformRating", 20));
            (resourceMap.get("CurrentMoney")).overrideValue(data.getInt("CurrentMoney", 0));
            (resourceMap.get("CurrentSteel")).overrideValue(data.getInt("CurrentSteel", 0));
            (resourceMap.get("CurrentTitanium")).overrideValue(data.getInt("CurrentTitanium", 0));
            (resourceMap.get("CurrentPlants")).overrideValue(data.getInt("CurrentPlants", 0));
            (resourceMap.get("CurrentEnergy")).overrideValue(data.getInt("CurrentEnergy", 0));
            (resourceMap.get("CurrentHeat")).overrideValue(data.getInt("CurrentHeat", 0));
            (resourceMap.get("ProductionMoney")).overrideValue(data.getInt("ProductionMoney", 0));
            (resourceMap.get("ProductionSteel")).overrideValue(data.getInt("ProductionSteel", 0));
            (resourceMap.get("ProductionTitanium")).overrideValue(data.getInt("ProductionTitanium", 0));
            (resourceMap.get("ProductionPlants")).overrideValue(data.getInt("ProductionPlants", 0));
            (resourceMap.get("ProductionEnergy")).overrideValue(data.getInt("ProductionEnergy", 0));
            (resourceMap.get("ProductionHeat")).overrideValue(data.getInt("ProductionHeat", 0));
            (resourceMap.get("Generation")).overrideValue(data.getInt("Generation", 1));
            steelValue = data.getInt("SteelValue", 2);
            titaniumValue = data.getInt("TitaniumValue", 3);
            plantsForAGreenery = data.getInt("PlantValue", 8);

            resourceMap.get("CurrentSteel").setValue(steelValue);
            resourceMap.get("CurrentTitanium").setValue(titaniumValue);

        }
        else
        {
            // Restore value of members from saved state

            (resourceMap.get("TerraformRating")).overrideValue(savedInstanceState.getInt("TerraformRating", 0));
            (resourceMap.get("CurrentMoney")).overrideValue(savedInstanceState.getInt("CurrentMoney", 0));
            (resourceMap.get("CurrentSteel")).overrideValue(savedInstanceState.getInt("CurrentSteel", 0));
            (resourceMap.get("CurrentTitanium")).overrideValue(savedInstanceState.getInt("CurrentTitanium", 0));
            (resourceMap.get("CurrentPlants")).overrideValue(savedInstanceState.getInt("CurrentPlants", 0));
            (resourceMap.get("CurrentEnergy")).overrideValue(savedInstanceState.getInt("CurrentEnergy", 0));
            (resourceMap.get("CurrentHeat")).overrideValue(savedInstanceState.getInt("CurrentHeat", 0));
            (resourceMap.get("ProductionMoney")).overrideValue(savedInstanceState.getInt("ProductionMoney", 0));
            (resourceMap.get("ProductionSteel")).overrideValue(savedInstanceState.getInt("ProductionSteel", 0));
            (resourceMap.get("ProductionTitanium")).overrideValue(savedInstanceState.getInt("ProductionTitanium", 0));
            (resourceMap.get("ProductionPlants")).overrideValue(savedInstanceState.getInt("ProductionPlants", 0));
            (resourceMap.get("ProductionEnergy")).overrideValue(savedInstanceState.getInt("ProductionEnergy", 0));
            (resourceMap.get("ProductionHeat")).overrideValue(savedInstanceState.getInt("ProductionHeat", 0));
            (resourceMap.get("Generation")).overrideValue(savedInstanceState.getInt("Generation", 1));
            steelValue = savedInstanceState.getInt("SteelValue", 2);
            titaniumValue = savedInstanceState.getInt("TitaniumValue", 3);
            plantsForAGreenery = savedInstanceState.getInt("PlantValue", 8);

            resourceMap.get("CurrentSteel").setValue(steelValue);
            resourceMap.get("CurrentTitanium").setValue(titaniumValue);
        }
    }

    public void onSaveInstanceState(Bundle savedInstanceState)
    {

        savedInstanceState.putInt("TerraformRating", (resourceMap.get("TerraformRating")).getModifiedAmount());
        savedInstanceState.putInt("CurrentMoney", (resourceMap.get("CurrentMoney")).getModifiedAmount());
        savedInstanceState.putInt("ProductionMoney", (resourceMap.get("ProductionMoney")).getModifiedAmount());
        savedInstanceState.putInt("CurrentSteel", (resourceMap.get("CurrentSteel")).getModifiedAmount());
        savedInstanceState.putInt("ProductionSteel", (resourceMap.get("ProductionSteel")).getModifiedAmount());
        savedInstanceState.putInt("CurrentTitanium", (resourceMap.get("CurrentTitanium")).getModifiedAmount());
        savedInstanceState.putInt("ProductionTitanium", (resourceMap.get("ProductionTitanium")).getModifiedAmount());
        savedInstanceState.putInt("CurrentPlants", (resourceMap.get("CurrentPlants")).getModifiedAmount());
        savedInstanceState.putInt("ProductionPlants", (resourceMap.get("ProductionPlants")).getModifiedAmount());
        savedInstanceState.putInt("CurrentEnergy", (resourceMap.get("CurrentEnergy")).getModifiedAmount());
        savedInstanceState.putInt("ProductionEnergy", (resourceMap.get("ProductionEnergy")).getModifiedAmount());
        savedInstanceState.putInt("CurrentHeat", (resourceMap.get("CurrentHeat")).getModifiedAmount());
        savedInstanceState.putInt("ProductionHeat", (resourceMap.get("ProductionHeat")).getModifiedAmount());
        savedInstanceState.putInt("Generation", (resourceMap.get("Generation")).getModifiedAmount());

        savedInstanceState.putInt("SteelValue", steelValue);
        savedInstanceState.putInt("TitaniumValue", titaniumValue);
        savedInstanceState.putInt("PlantValue", plantsForAGreenery);

    }

    public void onPause(Activity activity)
    {

        SharedPreferences data = activity.getSharedPreferences("TERRAFORMINGMARS", 0);
        SharedPreferences.Editor editor = data.edit();

        editor.putInt("TerraformRating", (resourceMap.get("TerraformRating")).getModifiedAmount());
        editor.putInt("CurrentMoney", (resourceMap.get("CurrentMoney")).getModifiedAmount());
        editor.putInt("ProductionMoney", (resourceMap.get("ProductionMoney")).getModifiedAmount());
        editor.putInt("CurrentSteel", (resourceMap.get("CurrentSteel")).getModifiedAmount());
        editor.putInt("ProductionSteel", (resourceMap.get("ProductionSteel")).getModifiedAmount());
        editor.putInt("CurrentTitanium", (resourceMap.get("CurrentTitanium")).getModifiedAmount());
        editor.putInt("ProductionTitanium", (resourceMap.get("ProductionTitanium")).getModifiedAmount());
        editor.putInt("CurrentPlants", (resourceMap.get("CurrentPlants")).getModifiedAmount());
        editor.putInt("ProductionPlants", (resourceMap.get("ProductionPlants")).getModifiedAmount());
        editor.putInt("CurrentEnergy", (resourceMap.get("CurrentEnergy")).getModifiedAmount());
        editor.putInt("ProductionEnergy", (resourceMap.get("ProductionEnergy")).getModifiedAmount());
        editor.putInt("CurrentHeat", (resourceMap.get("CurrentHeat")).getModifiedAmount());
        editor.putInt("ProductionHeat", (resourceMap.get("ProductionHeat")).getModifiedAmount());
        editor.putInt("Generation", (resourceMap.get("Generation")).getModifiedAmount());

        editor.putInt("SteelValue", steelValue);
        editor.putInt("TitaniumValue", titaniumValue);
        editor.putInt("PlantValue", plantsForAGreenery);

        editor.apply();

    }

}
