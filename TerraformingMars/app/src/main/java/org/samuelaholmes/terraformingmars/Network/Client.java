package org.samuelaholmes.terraformingmars.Network;

import android.os.AsyncTask;

import java.io.IOException;

import java.net.Socket;
import java.net.UnknownHostException;

public class Client extends AsyncTask<Void, Void, Socket> {

    String dstAddress;
    int dstPort;
    ClientInstance clientInstance;

    public Client(String addr, int port, ClientInstance clientInstance) {
        dstAddress = addr;
        dstPort = port;
        this.clientInstance = clientInstance;
    }

    @Override
    protected Socket doInBackground(Void... arg0) {

        Socket socket = null;

        try {
            socket = new Socket(dstAddress, dstPort);

        } catch (UnknownHostException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return socket;
    }

    @Override
    protected void onPostExecute(Socket result) {
        clientInstance.setSocket(result);
        super.onPostExecute(result);
    }

}
