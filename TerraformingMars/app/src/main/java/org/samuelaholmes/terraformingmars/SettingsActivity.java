package org.samuelaholmes.terraformingmars;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import org.w3c.dom.Text;

public class SettingsActivity extends AppCompatActivity {

    TextView steel;
    TextView titanium;
    TextView plants;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        setContentView(R.layout.settings_screen);

        steel = (TextView) findViewById(R.id.steelValue);
        titanium = (TextView) findViewById(R.id.titaniumValue);
        plants = (TextView) findViewById(R.id.plantValue);

        Intent intent = getIntent();
        steel.setText(getIntent().getStringExtra("SteelValue"));
        titanium.setText(getIntent().getStringExtra("TitaniumValue"));
        plants.setText(getIntent().getStringExtra("PlantValue"));

    }

    public void steelIncrease(View view)
    {
        int x = Integer.parseInt(steel.getText().toString());
        steel.setText(String.valueOf(x+1));
    }
    public void steelDecrease(View view)
    {
        int x = Integer.parseInt(steel.getText().toString());
        steel.setText(String.valueOf(x-1));
    }
    public void titaniumIncrease(View view)
    {
        int x = Integer.parseInt(titanium.getText().toString());
        titanium.setText(String.valueOf(x+1));
    }
    public void titaniumDecrease(View view)
    {
        int x = Integer.parseInt(titanium.getText().toString());
        titanium.setText(String.valueOf(x-1));
    }
    public void plantIncrease(View view)
    {
        int x = Integer.parseInt(plants.getText().toString());
        plants.setText(String.valueOf(x+1));
    }
    public void plantDecrease(View view)
    {
        int x = Integer.parseInt(plants.getText().toString());
        plants.setText(String.valueOf(x-1));
    }

    @Override
    public void onBackPressed()
    {


        Intent intent = new Intent();
        intent.putExtra("SteelValue", steel.getText());
        intent.putExtra("TitaniumValue", titanium.getText());
        intent.putExtra("PlantValue", plants.getText());
        setResult(0, intent);
        finish();
        super.onBackPressed();

    }

    @Override
    protected void onPause()
    {
        super.onPause();


    }

    @Override
    protected  void onStop()
    {


        super.onStop();
    }
}
