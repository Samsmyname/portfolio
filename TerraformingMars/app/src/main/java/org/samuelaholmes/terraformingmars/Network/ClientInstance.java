package org.samuelaholmes.terraformingmars.Network;

import org.samuelaholmes.terraformingmars.DataStructure;
import org.samuelaholmes.terraformingmars.MainActivity;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Timer;
import java.util.TimerTask;

public class ClientInstance {

    String addr;
    int port;
    Socket socket;
    MainActivity activity;
    final DataStructure myData;

    public ClientInstance(final String name, String addr, int port, final MainActivity activity, DataStructure mD)
    {
        this.activity = activity;
        this.addr = addr;
        this.port = port;

        this.socket = null;
        this.myData = mD;

        Client client = new Client(addr, port, this);
        client.execute();
    }

    public void setSocket(Socket socket)
    {
        this.socket = socket;
        activity.showConnected();
        SendThread sendThread = new SendThread(socket, activity, false, activity.name);
        ReceiveThread receiveThread = new ReceiveThread(socket, activity);
        sendThread.start();
        receiveThread.start();
    }

    public void onStop()
    {
        try {
            if (socket != null)
                socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }





}
