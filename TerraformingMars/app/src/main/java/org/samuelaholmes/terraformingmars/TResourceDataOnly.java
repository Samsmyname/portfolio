package org.samuelaholmes.terraformingmars;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.widget.TextView;

/**
 * Created by samue on 18/01/2018.
 */

public class TResourceDataOnly {

    private int amount;
    private int modifiedAmount;
    private int value = 1;

    public TResourceDataOnly(boolean selectable, int value)
    {
        this.value = value;
        reset();
    }

    public void modify(int x)
    {
        modifiedAmount += x;
    }

    public void confirmModify()
    {
        amount = modifiedAmount;
    }
    public void revertModify()
    {
        modifiedAmount = amount;
    }

    public boolean isModified()
    {
        if (amount == modifiedAmount)
            return false;
        return true;
    }

    public void reset()
    {
        amount = 0;
        modifiedAmount = 0;
    }

    public void overrideValue(int x)
    {
        modifiedAmount = x;
        confirmModify();
    }

    public int getModifiedDifference()
    {
        return (amount - modifiedAmount);
    }
    public int getModifiedDifferenceValue()
    {
        return (amount - modifiedAmount) * value;
    }
    public int getModifiedAmount()
    {
        return modifiedAmount;
    }
    public int getAmount(){return amount;}
    public void setValue(int v){this.value=v;}
    public int getValue(){return this.value;}

}
