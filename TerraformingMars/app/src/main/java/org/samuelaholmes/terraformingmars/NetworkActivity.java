package org.samuelaholmes.terraformingmars;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import org.samuelaholmes.terraformingmars.Network.Server;

/**
 * Created by samue on 6/07/2018.
 */

public class NetworkActivity extends Fragment {
    TextView infoip, msg;
    public View rootView;
    MainActivity activity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.chat_test, container, false);

        infoip = (TextView) rootView.findViewById(R.id.infoip);
        msg = (TextView) rootView.findViewById(R.id.msg);


        //mStatusView.setText(text);

        if (activity.host())
            infoip.setText(activity.server.getIpAddress() + ":" + activity.server.getPort());

        return rootView;
    }

    public TextView getTextView()
    {
        return msg;
    }

    public void setActivity(MainActivity activity)
    {
        this.activity = activity;
    }

    public void onStartServer(Server server)
    {

    }


}
