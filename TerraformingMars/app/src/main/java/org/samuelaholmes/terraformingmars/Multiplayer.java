package org.samuelaholmes.terraformingmars;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.samuelaholmes.terraformingmars.Network.ClientInstance;

/**
 * Created by samue on 3/07/2018.
 */

public class Multiplayer extends Fragment {

    EditText editTextAddress, editTextPort, editTextName;
    Button buttonConnect, buttonHost;
    ViewGroup multiplayerGrid;
    ViewGroup multiplayerButtons;
    MainActivity activity;
    String activeName = "";

    public boolean active = false;

    DataStructure myData;

    public static final String ARG_OBJECT = "object";

    private ClientInstance clientInstance;

    public void attachData(DataStructure data, MainActivity activity)
    {
        this.myData = data;
        this.activity = activity;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.multiplayer_screen, container, false);

        editTextAddress = (EditText) rootView.findViewById(R.id.addressEditText);
        editTextPort = (EditText) rootView.findViewById(R.id.portEditText);
        editTextName = (EditText) rootView.findViewById(R.id.nameEditText);
        buttonConnect = (Button) rootView.findViewById(R.id.connectButton);
        buttonHost = (Button) rootView.findViewById(R.id.host_button);
        multiplayerGrid = (ViewGroup) rootView.findViewById(R.id.multiplayer_grid);
        multiplayerButtons = (ViewGroup) rootView.findViewById(R.id.buttons);




        buttonConnect.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                activity.name = editTextName.getText().toString();
                clientInstance = new ClientInstance(activity.name, editTextAddress.getText()
                        .toString(), Integer.parseInt(editTextPort
                        .getText().toString()), activity, myData);
                active = true;
            }
        });
        buttonHost.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                activity.name = editTextName.getText().toString();
                if (activity.host())
                    Toast.makeText(getContext(), activity.server.getIpAddress() + ":" + activity.server.getPort(), Toast.LENGTH_SHORT).show();

                active = true;
            }
        });


        showButtons();

        return rootView;
    }

    private void showActive()
    {
        multiplayerGrid.removeAllViews();
        if (!activeName.equals(""))
        {

            DataStructure data = activity.multiPlayerData.get(activeName);
            LayoutInflater factory = LayoutInflater.from(getContext());
            View myView = factory.inflate(R.layout.multiplayer_view, null);
            multiplayerGrid.addView(myView);

            TextView terraformRatingv = (TextView) myView.findViewById(R.id.terraformRating);
            TextView moneyCurrentv = (TextView) myView.findViewById(R.id.moneyCurrent);
            TextView moneyProductionv = (TextView) myView.findViewById(R.id.moneyProduction);
            TextView steelCurrentv = (TextView) myView.findViewById(R.id.steelCurrent);
            TextView steelProductionv = (TextView) myView.findViewById(R.id.steelProduction);
            TextView titaniumCurrentv = (TextView) myView.findViewById(R.id.titaniumCurrent);
            TextView titaniumProductionv = (TextView) myView.findViewById(R.id.titaniumProduction);
            TextView plantsCurrentv = (TextView) myView.findViewById(R.id.plantsCurrent);
            TextView plantsProductionv = (TextView) myView.findViewById(R.id.plantsProduction);
            TextView energyCurrentv = (TextView) myView.findViewById(R.id.energyCurrent);
            TextView energyProductionv = (TextView) myView.findViewById(R.id.energyProduction);
            TextView heatCurrentv = (TextView) myView.findViewById(R.id.heatCurrent);
            TextView heatProductionv = (TextView) myView.findViewById(R.id.heatProduction);
            TextView generationv = (TextView) myView.findViewById(R.id.generation);

            terraformRatingv.setText(String.valueOf(data.resourceMap.get("TerraformRating").getAmount()));
            moneyCurrentv.setText(String.valueOf(data.resourceMap.get("CurrentMoney").getAmount()));
            moneyProductionv.setText(String.valueOf(data.resourceMap.get("ProductionMoney").getAmount()));
            steelCurrentv.setText(String.valueOf(data.resourceMap.get("CurrentSteel").getAmount()));
            steelProductionv.setText(String.valueOf(data.resourceMap.get("ProductionSteel").getAmount()));
            titaniumCurrentv.setText(String.valueOf(data.resourceMap.get("CurrentTitanium").getAmount()));
            titaniumProductionv.setText(String.valueOf(data.resourceMap.get("ProductionTitanium").getAmount()));
            plantsCurrentv.setText(String.valueOf(data.resourceMap.get("CurrentPlants").getAmount()));
            plantsProductionv.setText(String.valueOf(data.resourceMap.get("ProductionPlants").getAmount()));
            energyCurrentv.setText(String.valueOf(data.resourceMap.get("CurrentEnergy").getAmount()));
            energyProductionv.setText(String.valueOf(data.resourceMap.get("ProductionEnergy").getAmount()));
            heatCurrentv.setText(String.valueOf(data.resourceMap.get("CurrentHeat").getAmount()));
            heatProductionv.setText(String.valueOf(data.resourceMap.get("ProductionHeat").getAmount()));
            generationv.setText(String.valueOf(data.resourceMap.get("Generation").getAmount()));
        }
    }

    public void showDisconnected()
    {
        activeName = "";
        activity.multiPlayerData.clear();
        showActive();
        showButtons();
        buttonConnect.setBackgroundColor(Color.RED);

    }

    private void showButtons()
    {
        multiplayerButtons.removeAllViews();
        for (final String iname : activity.multiPlayerData.keySet())
        {
            Button b = new Button(getContext());
            b.setTransformationMethod(null);
            b.setOnClickListener(new Button.OnClickListener(){
                @Override
                public void onClick(View v) {
                    activeName = ((Button)v).getText().toString();
                    showButtons();
                }
            });
            b.setText(iname);
            if (iname.equals(activeName))
                b.setBackgroundColor(Color.GREEN);
            if (!iname.equals(activity.name))
                multiplayerButtons.addView(b);
            showActive();
        }
    }

    public void displayButtons()
    {

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showButtons();
            }
        });




    }

    public void reset()
    {

    }

    public void onStopObject()
    {
        if (clientInstance != null)
            clientInstance.onStop();
    }


}
