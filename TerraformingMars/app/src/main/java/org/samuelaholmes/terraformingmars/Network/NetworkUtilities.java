package org.samuelaholmes.terraformingmars.Network;

import android.util.Log;

import org.samuelaholmes.terraformingmars.DataStructure;
import org.samuelaholmes.terraformingmars.MainActivity;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.HashMap;

public class NetworkUtilities {

    public static void SendData(Socket socket, HashMap<String, DataStructure> multiplayerData)
    {
        for (String player : multiplayerData.keySet()) {
            SendData(socket, player, multiplayerData.get(player));
        }
    }

    public static void SendData(Socket socket, String senderName, DataStructure myData) {

        if (socket != null)
            try {

                final OutputStream outputStream = socket.getOutputStream();
                PrintStream printStream = new PrintStream(outputStream);

                printStream.print(myData.constructMessage(senderName) + ";");

            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
    }

    public static void SendShutDownSignal(Socket socket)
    {
        if (socket != null)
            try {

                final OutputStream outputStream = socket.getOutputStream();
                PrintStream printStream = new PrintStream(outputStream);

                printStream.print("---" + ";");

            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
    }

    public static void ReceiveData(Socket socket, HashMap<String, DataStructure> multiPlayerData)
    {
        if (socket != null)
            try {
                final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(1024);
                final byte[] buffer = new byte[1024];
                final InputStream inputStream = socket.getInputStream();
                int bytesRead;
                String rsp = "";

                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    byteArrayOutputStream.write(buffer, 0, bytesRead);
                    rsp += byteArrayOutputStream.toString("UTF-8");
                    String t = rsp.substring(rsp.length() - 1);
                    if (t .equals(";"))
                        break;
                }

                if (!rsp.equals("")) {
                    final String[] messages = rsp.split(";");
                    for (String message : messages)
                    {
                        String code = message.substring(0,3);
                        message = message.substring(3);
                        if (code.equals("-D-")) {
                            final String[] multi1 = message.split("\\*");
                            if (!multiPlayerData.containsKey(multi1[0])) {
                                multiPlayerData.put(multi1[0], new DataStructure());
                            }
                            String[] multi2 = multi1[1].split(";");
                            for (String replaced : multi2) {
                                final String output = replaced.replace(";", "");
                                multiPlayerData.get(multi1[0]).deconstructMessage(output);
                            }
                        }
                        if (code.equals("---"))
                        {
                            socket.close();
                        }
                    }
                }

            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (SocketException e) {
                try {
                    socket.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
    }
}
