package org.samuelaholmes.terraformingmars.Network;

import org.samuelaholmes.terraformingmars.MainActivity;

import java.net.Socket;
import java.util.concurrent.TimeUnit;

public class SendThread extends Thread {
    Socket socket;
    MainActivity activity;
    boolean server;
    String name;

    SendThread(Socket socket, MainActivity activity, boolean server, String name) {
        this.socket = socket;
        this.activity = activity;
        this.server = server;
        this.name = name;
    }

    @Override
    public void run() {
        try {
            if (socket != null && !socket.isClosed()) {
                if (server)
                    NetworkUtilities.SendData(socket, activity.multiPlayerData);
                NetworkUtilities.SendData(socket, name, activity.myData);
                TimeUnit.SECONDS.sleep(1);
                if (!activity.shuttingDown)
                    this.run();
                else {
                    NetworkUtilities.SendShutDownSignal(socket);
                }
            }
            else
            {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        activity.showDisconnected();
                    }
                });

            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
