package org.samuelaholmes.terraformingmars;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by samue on 3/07/2018.
 */

public class TransactionTable extends Fragment {


    public static final String ARG_OBJECT = "object";
    View rootView;
    DataStructure myData;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.transaction_table, container, false);


        FillTable();


        return rootView;
    }

    public void attachData(DataStructure myData)
    {
        this.myData = myData;
    }

    public void FillTable()
    {
        boolean canChange = true;
        ArrayList<Transaction> transactions = myData.transactions;
        final TableLayout tl = (TableLayout) rootView.findViewById(R.id.table);
        tl.removeAllViews();

        for (int j = transactions.size()-1; j>=0; j--) {
            Transaction t = transactions.get(j);
            TableRow tr = new TableRow(getContext());
            tr.setLayoutParams(new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT));

            TextView tv = new TextView(getContext());
            tv.setMinimumHeight(200);
            GradientDrawable border = new GradientDrawable();
            int c = Color.argb(50, 255, 255, 255);
            border.setColor(c);
            border.setStroke(8, 0xFF000000); //black border with full opacity
            tv.setBackground(border);
            tv.setLayoutParams(new TableRow.LayoutParams(0, TableRow.LayoutParams.MATCH_PARENT, 2));

            String string = "";

            if (t.g == 1)
            {
                tv.setText("*New Generation*");
                tr.addView(tv);
                canChange = false;
            }
            else {
                if (t.trChange != 0)
                    string += "TR: " + t.trChange * -1 + "\n";
                if (t.moneyChange != 0)
                    string += "Money: " + t.moneyChange * -1 + "\n";
                if (t.moneyProductionchange != 0)
                    string += "Money Income: " + t.moneyProductionchange * -1 + "\n";
                if (t.steelChange != 0)
                    string += "Steel: " + t.steelChange * -1 + "\n";
                if (t.steelProductionChange != 0)
                    string += "Steel Prod: " + t.steelProductionChange * -1 + "\n";
                if (t.titaniumChange != 0)
                    string += "Titanium: " + t.titaniumChange * -1 + "\n";
                if (t.titaniumProductionChange != 0)
                    string += "Titanium Prod: " + t.titaniumProductionChange * -1 + "\n";
                if (t.plantsChange != 0)
                    string += "Plants: " + t.plantsChange * -1 + "\n";
                if (t.plantsProductionChange != 0)
                    string += "Plants Prod: " + t.plantsProductionChange * -1 + "\n";
                if (t.powerChange != 0)
                    string += "Power: " + t.powerChange * -1 + "\n";
                if (t.powerProductionChange != 0)
                    string += "Power Prod: " + t.powerProductionChange * -1 + "\n";
                if (t.heatChange != 0)
                    string += "Heat: " + t.heatChange * -1 + "\n";
                if (t.heatProductionChange != 0)
                    string += "Heat Prod: " + t.heatProductionChange * -1 + "\n";

                string = string.trim();

                tv.setText(string);

                tr.addView(tv);

                final int i = transactions.indexOf(t);

                Button b = new Button(getContext());
                b.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        myData.revertTranscation(i);
                        FillTable();
                    }
                });
                b.setBackground(border);
                b.setLayoutParams(new TableRow.LayoutParams(0, TableRow.LayoutParams.MATCH_PARENT, 1));
                b.setText("Undo");

                if (canChange)
                    tr.addView(b);
            }
            tl.addView(tr);


        }
    }


}
